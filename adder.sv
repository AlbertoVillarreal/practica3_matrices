module adder
#(
	parameter WORD_LENGTH = 8
)
(
	//inputs
	input [WORD_LENGTH - 1 : 0]data_1,
	input [WORD_LENGTH - 1 : 0]data_2,
	
	//outputs
	output [WORD_LENGTH - 1 : 0]data_output
);
	logic [WORD_LENGTH - 1 : 0]data_output_wire;
	always_comb
	begin
		data_output_wire = data_1 + data_2;
	end
	
	assign data_output = data_output_wire;
endmodule