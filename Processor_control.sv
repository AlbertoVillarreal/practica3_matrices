module Processor_control
#(
	parameter VALUE_TO_COUNT_LENGTH = 4,
	parameter TOTAL_VALUE_TO_COUNT = 5,
	parameter SELECTOR_LENGTH = 3
)
(
	//Inputs
	input 												start_control,
	input													clk,
	input													reset,
	input[VALUE_TO_COUNT_LENGTH - 1 : 0]		matriz_size,
	//Outpus
	//Enable processors outputs
	output bit 											enable_first_processor,
	output bit 											enable_second_processor,
	output bit 											enable_third_processor,
	output bit 											enable_fourth_processor,
	//Counter outputs
	output bit 											enable_total_counter,
	output [TOTAL_VALUE_TO_COUNT - 1 : 0] 	 	total_count_cycle
	  	
);
enum logic[2:0] {IDLE, WICH_PROCESSORS,SET_COUNTERS, FIRST_PROCESSOR_E, SECOND_PROCESSOR_E, THIRD_PROCESSOR_E,
					  FOURTH_PROCESSOR_E} state, next_state;
//Enable processors wires 
bit enable_first_processor_wire;
bit enable_second_processor_wire;
bit enable_third_processor_wire;
bit enable_fourth_processor_wire;
//

//Counter wires
bit 	enable_total_counter_wire;
logic [TOTAL_VALUE_TO_COUNT - 1 : 0]	total_count_cycle_wire;

always_ff@(posedge clk or negedge reset)
begin
	if(reset == 1'b0)
		state <= IDLE;
	else
		state <= next_state;
end
		
always_comb
begin
	case(state)
		IDLE:
		begin
			if(start_control)
				next_state = SET_COUNTERS;
			else
				next_state = IDLE;
		end
		WICH_PROCESSORS:
		begin
			next_state = SET_COUNTERS;
		end
		SET_COUNTERS:
		begin
			next_state = FIRST_PROCESSOR_E;
		end
		
		FIRST_PROCESSOR_E:
		begin
			if(matriz_size == 1)
				next_state = IDLE;
			else
				next_state = SECOND_PROCESSOR_E;
		end
			
		SECOND_PROCESSOR_E:
		begin
			if(matriz_size == 2)
				next_state = IDLE;
			else
				next_state = THIRD_PROCESSOR_E;
		end
		
		THIRD_PROCESSOR_E:
		begin
			if(matriz_size == 3)
				next_state = IDLE;
			else
				next_state = FOURTH_PROCESSOR_E;
		end
		
		FOURTH_PROCESSOR_E:
		begin
			next_state = IDLE;
		end
		
		endcase
end

always_comb
begin
	enable_total_counter_wire = 0;
	enable_first_processor_wire = 0;
	enable_second_processor_wire = 0;
	enable_third_processor_wire = 0;
	enable_fourth_processor_wire = 0;

	if(matriz_size <= 4)
		total_count_cycle_wire = ((matriz_size * 2) - 1);
	else
		total_count_cycle_wire = ((matriz_size * 2) + 3);
	case(state)
		IDLE:
		begin
		end
		
		SET_COUNTERS:
		begin
			enable_total_counter_wire = 1;
		end
		
		FIRST_PROCESSOR_E:
		begin
			enable_first_processor_wire = 1;				
		end
		
		SECOND_PROCESSOR_E:
		begin
			enable_second_processor_wire = 1;
		end
		
		THIRD_PROCESSOR_E:
		begin
			enable_third_processor_wire = 1;
		end
		
		FOURTH_PROCESSOR_E:
		begin
			enable_fourth_processor_wire = 1;
		end
		
		default:
		begin
			enable_total_counter_wire = 0;
			enable_first_processor_wire = 0;
			enable_second_processor_wire = 0;
			enable_third_processor_wire = 0;
			enable_fourth_processor_wire = 0;
		end
	endcase
end

assign enable_first_processor = enable_first_processor_wire;
assign enable_second_processor = enable_second_processor_wire;
assign enable_third_processor = enable_third_processor_wire;
assign enable_fourth_processor = enable_fourth_processor_wire;

assign enable_total_counter = enable_total_counter_wire;
assign total_count_cycle = total_count_cycle_wire;

endmodule