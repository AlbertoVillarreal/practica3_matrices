module DataFeederControl
#(
	parameter Word_Length = 8
)
(
	//Input ports
	input clk,
	input reset,
	input individualFIFOCounterFlag,				//Counter de FIFOs individuales
	input totalFIFOsSizeFlag,						//Counter que se activa cuando se llenan todas las FIFOs
	input calculusReady,								//Senial de control que indica cuando el calculo este listo
	input matrixOrVector,							//Senial para elegir entre llenar matriz o vector (senial de registro de matrixLoaded)
	input isVectorLoaded,							//Senial que indica si el vector ya fue llenado
	input generalFIFO_emptyFlag,					//Senial de empty de general FIFO
	input [Word_Length - 1 : 0]DataFromFIFO,	//Datos de general FIFO
	
	//Output ports
	output matrixLoaded,
	output vectorLoaded,
	output pushIndividualFifo,
	output popGeneralFifo,
	output startindividualFIFOCounter,
	output startCalculus,
	output retransmit,
	output syncReset,
	output enableMatrixSizeRegister,
	output [Word_Length - 5 : 0]demuxSelector

);

/* States */
enum logic [5 : 0] {IDLE, FE, LENGTH, CMD, SIZE, RETRANSMIT, EF, FIFO1, FIFO2, FIFO3, FIFO4, FIFO5, FIFO6, FIFO7, FIFO8, READY, FIFO_VECTOR, RESET, END, START_CALCULUS} state, next_state;

logic matrixLoaded_bit;
logic vectorLoaded_bit;
logic popGeneralFifo_bit;
logic startindividualFIFOCounter_bit;
logic startCalculus_bit;
logic retransmit_bit;
logic syncReset_bit;
logic pushIndividualFifo_logic;
logic enableMatrixSizeRegister_logic;
logic [Word_Length - 5 : 0] demuxSelector_logic;


	//state assignation, secuential process
always_ff@(posedge clk or negedge reset)
begin
	if(reset == 1'b0)
		state <= IDLE;
	else
		state <= next_state;
end

//state assignaation, combinational process
always_comb
begin
	
	case(state)
		IDLE:
		begin
			if(generalFIFO_emptyFlag == 1'b0)
				next_state = FE;
			else
				next_state = IDLE;
		end
		FE:
		begin
			if(DataFromFIFO == 8'hFE)
				next_state = LENGTH;
			else
				next_state = FE;
		end
		LENGTH:
			next_state = CMD;
			
		CMD:
		begin
			if(DataFromFIFO == 8'h01)											//recibimos tamanio de la matriz a procesar
				next_state = SIZE;
				
			else if(DataFromFIFO == 8'h02)									//Retransmitir el resultado
				next_state = RETRANSMIT;
			
			else if(DataFromFIFO == 8'h03)									//Iniciamos la recepcion
				next_state = EF;
			
			else if(DataFromFIFO == 8'h04 && matrixOrVector == 1'b0)	//Recibimos matriz
				next_state = FIFO1;
			
			else if(DataFromFIFO == 8'h04 && matrixOrVector == 1'b1)	//Recibimos vector
				next_state = FIFO_VECTOR;
				
			else
				next_state = IDLE;
			
		end
		
	/*Procesamiento interno*/
		SIZE:
			next_state = EF;
			
		RETRANSMIT:
		begin
			if(calculusReady == 1'b0)
				next_state = RETRANSMIT;
				
			else
				next_state = EF;
		end
		
		FIFO1:
		begin
			if(individualFIFOCounterFlag == 1'b1 && totalFIFOsSizeFlag == 1'b0)
				next_state = FIFO2;
				
			else if(individualFIFOCounterFlag == 1'b1 && totalFIFOsSizeFlag == 1'b1)
				next_state = READY;
				
			else
				next_state = FIFO1;
		end
		
		FIFO2:
		begin
			if(individualFIFOCounterFlag == 1'b1 && totalFIFOsSizeFlag == 1'b0)
				next_state = FIFO3;
				
			else if(individualFIFOCounterFlag == 1'b1 && totalFIFOsSizeFlag == 1'b1)
				next_state = READY;
				
			else
				next_state = FIFO2;
		end
		
		FIFO3:
		begin
			if(individualFIFOCounterFlag == 1'b1 && totalFIFOsSizeFlag == 1'b0)
				next_state = FIFO4;
				
			else if(individualFIFOCounterFlag == 1'b1 && totalFIFOsSizeFlag == 1'b1)
				next_state = READY;
				
			else
				next_state = FIFO3;
		end
		
		FIFO4:
		begin
			if(individualFIFOCounterFlag == 1'b1 && totalFIFOsSizeFlag == 1'b0)
				next_state = FIFO5;
				
			else if(individualFIFOCounterFlag == 1'b1 && totalFIFOsSizeFlag == 1'b1)
				next_state = READY;
				
			else
				next_state = FIFO4;
		end

		FIFO5:
		begin
			if(individualFIFOCounterFlag == 1'b1 && totalFIFOsSizeFlag == 1'b0)
				next_state = FIFO6;
				
			else if(individualFIFOCounterFlag == 1'b1 && totalFIFOsSizeFlag == 1'b1)
				next_state = READY;
				
			else
				next_state = FIFO5;
		end
		
		FIFO6:
		begin
			if(individualFIFOCounterFlag == 1'b1 && totalFIFOsSizeFlag == 1'b0)
				next_state = FIFO7;
				
			else if(individualFIFOCounterFlag == 1'b1 && totalFIFOsSizeFlag == 1'b1)
				next_state = READY;
				
			else
				next_state = FIFO6;
		end
		
		FIFO7:
		begin
			if(individualFIFOCounterFlag == 1'b1 && totalFIFOsSizeFlag == 1'b0)
				next_state = FIFO8;
				
			else if(individualFIFOCounterFlag == 1'b1 && totalFIFOsSizeFlag == 1'b1)
				next_state = READY;
				
			else
				next_state = FIFO7;
		end
		
		FIFO8:
		begin	
			if(individualFIFOCounterFlag == 1'b1)
				next_state = READY;
				
			else
				next_state = FIFO8;
		end
		
		READY:
		begin
			next_state = FE;
		end

		FIFO_VECTOR:
		begin	
			if(individualFIFOCounterFlag == 1'b1)
				next_state = EF;
				
			else
				next_state = FIFO_VECTOR;
		end
		
	/*Resto del paquete*/	
		EF:
		begin
			if(DataFromFIFO == 8'hEF)
				next_state = END;
			else
				next_state = RESET;
		end	
		
		END:
		begin
			if(matrixOrVector == 1'b1 && isVectorLoaded == 1'b1)
				next_state = START_CALCULUS;
			else
				next_state = IDLE;
		end
		
		START_CALCULUS:
		begin
				next_state = IDLE;
		end
		
		RESET:
		begin
			if(generalFIFO_emptyFlag == 1'b1)
				next_state = IDLE;
			else
				next_state = RESET;
		end
				
		
	endcase
end


//output assignaation, combinational process
always_comb
begin
	matrixLoaded_bit = 1'b0;
	vectorLoaded_bit = 1'b0;
	pushIndividualFifo_logic = 1'b0;
	popGeneralFifo_bit = 1'b1;											//Sacamos dato cada ciclo a menos que indiquemos lo contrario			
	startindividualFIFOCounter_bit = 1'b0;
	startCalculus_bit = 1'b0;
	retransmit_bit = 1'b0;
	syncReset_bit = 1'b0;
	enableMatrixSizeRegister_logic = 1'b0; 
	demuxSelector_logic = 4'b1111;

	case(state)
		IDLE:
		begin
			popGeneralFifo_bit = 1'b0;
		end
		FE:
		begin
			popGeneralFifo_bit = 1'b0;
		end
		LENGTH:
		begin
			
		end
			
		CMD:
		begin
			
		end
		
	/*Procesamiento interno*/
		SIZE:
		begin
			enableMatrixSizeRegister_logic = 1'b1;
		end
			
		RETRANSMIT:
		begin
			retransmit_bit = 1'b1;
		end
		
		FIFO1:
		begin
			pushIndividualFifo_logic = 1'b1;
			startindividualFIFOCounter_bit = 1'b1;
			demuxSelector_logic = 4'b0000;
		end
		
		FIFO2:
		begin
			pushIndividualFifo_logic = 1'b1;
			startindividualFIFOCounter_bit = 1'b1;
			demuxSelector_logic = 4'b0001;
		end
		
		FIFO3:
		begin
			pushIndividualFifo_logic = 1'b1;
			startindividualFIFOCounter_bit = 1'b1;
			demuxSelector_logic = 4'b0010;
		end
		
		FIFO4:
		begin
			pushIndividualFifo_logic = 1'b1;
			startindividualFIFOCounter_bit = 1'b1;
			demuxSelector_logic = 4'b0011;
		end

		FIFO5:
		begin
			pushIndividualFifo_logic = 1'b1;
			startindividualFIFOCounter_bit = 1'b1;
			demuxSelector_logic = 4'b0100;
		end
		
		FIFO6:
		begin
			pushIndividualFifo_logic = 1'b1;
			startindividualFIFOCounter_bit = 1'b1;
			demuxSelector_logic = 4'b0101;
		end
		
		FIFO7:
		begin
			pushIndividualFifo_logic = 1'b1;
			startindividualFIFOCounter_bit = 1'b1;
			demuxSelector_logic = 4'b0110;
		end
		
		FIFO8:
		begin	
			pushIndividualFifo_logic = 1'b1;
			startindividualFIFOCounter_bit = 1'b1;
			demuxSelector_logic = 4'b0111;
		end
		
		READY:
		begin
			matrixLoaded_bit = 1'b1;
			//popGeneralFifo_bit = 1'b0;
		end

		FIFO_VECTOR:
		begin	
			pushIndividualFifo_logic = 1'b1;
			startindividualFIFOCounter_bit = 1'b1;
			demuxSelector_logic = 4'b1000;
			vectorLoaded_bit = 1'b1;
		end
		
	/*Resto del paquete*/	
		EF:
		begin
			
		end	
		
		END:
		begin
			popGeneralFifo_bit = 1'b0;
		end
		
		START_CALCULUS:
		begin
			startCalculus_bit = 1'b1;
			popGeneralFifo_bit = 1'b0;
		end
		
		RESET:
		begin
			syncReset_bit = 1'b1;
			popGeneralFifo_bit = 1'b0;
		end
		
	endcase
end


assign matrixLoaded = matrixLoaded_bit;
assign vectorLoaded = vectorLoaded_bit;
assign popGeneralFifo = popGeneralFifo_bit;
assign demuxSelector = demuxSelector_logic;
assign startindividualFIFOCounter = startindividualFIFOCounter_bit;
assign startCalculus = startCalculus_bit;
assign retransmit = retransmit_bit;
assign syncReset = syncReset_bit;
assign pushIndividualFifo = pushIndividualFifo_logic;
assign enableMatrixSizeRegister = enableMatrixSizeRegister_logic;

endmodule