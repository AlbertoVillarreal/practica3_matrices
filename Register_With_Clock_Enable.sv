 module Register_With_Clock_Enable
#(
	parameter WORD_LENGTH = 8
)
(
	// Input Ports
	input clk,
	input reset,
	input syncReset,
	input [WORD_LENGTH-1:0] Data_Input,
	input enable, 
	
	// Output Ports
	output logic [WORD_LENGTH-1:0] Data_Output
);

logic [WORD_LENGTH-1:0] Data_logic;

always_ff@(posedge clk or negedge reset) begin:ThisIsARegister
	if(reset == 1'b0) 
		Data_logic <= {WORD_LENGTH{1'b0}};
	else if(syncReset == 1)
		Data_logic <= {WORD_LENGTH{1'b0}};
	else 
	begin
		if (enable == 1'b1)
			Data_logic <= Data_Input;
	end
end:ThisIsARegister

assign Data_Output = Data_logic;
endmodule