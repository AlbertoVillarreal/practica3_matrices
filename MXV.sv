module MXV
#(
	parameter WORD_LENGTH = 8, 				//Number of bits of data
	parameter DATA_COUNTER_FOR_RAM = 3
)
(
	//Input Ports
	input clk,
	input reset,
	input SerialData_Rx,

	//Output Ports
	output SerialData_Tx
);

	bit popSignal_1_5_log;
	bit popSignal_2_6_log;
	bit popSignal_3_7_log;
	bit popSignal_4_8_log;
	
	bit popSignal_1_log;
	bit popSignal_2_log;
	bit popSignal_3_log;
	bit popSignal_4_log;
	bit popSignal_5_log;
	bit popSignal_6_log;
	bit popSignal_7_log;
	bit popSignal_8_log;
	
	bit vector_pop_log;
	bit vector1_pop_log;
	bit vector2_pop_log;
	
	bit calculusReady_log;
	
	bit selector_between_1_and_5_log;
	bit selector_between_2_and_6_log;
	bit selector_between_3_and_7_log;
	bit selector_between_4_and_8_log;
	
	bit startCalculus_log;
	bit retransmit_log;
	bit pushFIFO1_log;
	bit pushFIFO2_log;
	bit pushFIFO3_log;
	bit pushFIFO4_log;
	bit pushFIFO5_log;
	bit pushFIFO6_log;
	bit pushFIFO7_log;
	bit pushFIFO8_log;
	bit pushVector_log;
	
	logic [WORD_LENGTH - 1 : 0] dataFIFO1_log;
	logic [WORD_LENGTH - 1 : 0] dataFIFO2_log;
	logic [WORD_LENGTH - 1 : 0] dataFIFO3_log;
	logic [WORD_LENGTH - 1 : 0] dataFIFO4_log;
	logic [WORD_LENGTH - 1 : 0] dataFIFO5_log;
	logic [WORD_LENGTH - 1 : 0] dataFIFO6_log;
	logic [WORD_LENGTH - 1 : 0] dataFIFO7_log;
	logic [WORD_LENGTH - 1 : 0] dataFIFO8_log;
	logic [WORD_LENGTH - 1 : 0] dataVector_log;
	logic [WORD_LENGTH - 1 : 0] matrixSize_log;
	
	logic [WORD_LENGTH - 1 : 0] FIFO1_DataOut;
	logic [WORD_LENGTH - 1 : 0] FIFO2_DataOut;
	logic [WORD_LENGTH - 1 : 0] FIFO3_DataOut;
	logic [WORD_LENGTH - 1 : 0] FIFO4_DataOut;
	logic [WORD_LENGTH - 1 : 0] FIFO5_DataOut;
	logic [WORD_LENGTH - 1 : 0] FIFO6_DataOut;
	logic [WORD_LENGTH - 1 : 0] FIFO7_DataOut;
	logic [WORD_LENGTH - 1 : 0] FIFO8_DataOut;
	logic [WORD_LENGTH - 1 : 0] Vector_DataOut;
	logic [WORD_LENGTH - 1 : 0] Vector1_DataOut;
	logic [WORD_LENGTH - 1 : 0] Vector2_DataOut;

	//Processor array output wires
	logic [WORD_LENGTH - 1 : 0] first_processor_output_wire;
	logic [WORD_LENGTH - 1 : 0] second_processor_output_wire;
	logic [WORD_LENGTH - 1 : 0] third_processor_output_wire;
	logic [WORD_LENGTH - 1 : 0] fourth_processor_output_wire;
	
	//Data colector outputs wires
	logic [WORD_LENGTH - 1 : 0] register1_out_wire_DC;
	logic [WORD_LENGTH - 1 : 0] register2_out_wire_DC;
	logic [WORD_LENGTH - 1 : 0] register3_out_wire_DC;
	logic [WORD_LENGTH - 1 : 0] register4_out_wire_DC;
	logic [WORD_LENGTH - 1 : 0] register5_out_wire_DC;
	logic [WORD_LENGTH - 1 : 0] register6_out_wire_DC;
	logic [WORD_LENGTH - 1 : 0] register7_out_wire_DC;
	logic [WORD_LENGTH - 1 : 0] register8_out_wire_DC;
	
	//Data output to send wires 
	bit counter_send_enable_wire;
	bit counter_flag_DS_wire;
	logic [3:0]counter_DS_out_wire;
	logic [WORD_LENGTH - 1 : 0] data_to_send_wire;
	
	//Enables for data colectoe enables
	bit register_data1and5_enable_wire;
	bit register_data2and6_enable_wire;
	bit register_data3and7_enable_wire;
	bit register_data4and8_enable_wire;
	
	//UART wires
	bit UART_Interrupt_bit;
	logic [WORD_LENGTH - 1 : 0] UART_Received_Data;
	logic [WORD_LENGTH - 1 : 0] UART_Data_to_Transmit;
	bit oneShot_transmit_signal_wire;
	
	//Clock's
	bit uartClk2;
	bit uartClk;
	bit Clk_1kHz;
	bit PLL_to_ClkDivider;
	
/*********************************************************************************************************************************/
/**********************************************        System clocks        ******************************************************/
/*********************************************************************************************************************************/	
//Agregar PLL de 1k, 115200 y 230400 para el sistema

PLL_10k	
PLL_10k_inst 
(
	.inclk0 ( clk ),
	.c0 ( PLL_to_ClkDivider )
);

PL_115200BR	
PLL_115200BR_inst 
(
	.inclk0 ( clk ),
	.c0 ( uartClk )
);

PL_230400BR	
PLL_230400BR_inst 
(
	.inclk0 ( clk ),
	.c0 ( uartClk2 )
);


ClockDivider
#(

	.FREQUENCY(1_000),
	.REFERENCE_FREQUENCY(10_000)
)
CLK_GENERATOR_1k
(
	// Input Ports
	.clk_FPGA(PLL_to_ClkDivider),
	.reset(reset),
	.enable(1'b1),
	
	// Output Ports
	.Clock_Signal(Clk_1kHz)
);
	
/*********************************************************************************************************************************/
/**************************************************        UART        ***********************************************************/
/*********************************************************************************************************************************/	
UART
UART_TX_RX
(
	//Input Ports
	.clk_230400(uartClk2),
	.clk_115200(uartClk),
	.reset(reset),
	.Transmit(oneShot_transmit_signal_wire), 
	.SerialData_Rx(SerialData_Rx),
	.Clear_Interrupt(UART_Interrupt_bit),
	.Data_to_Transmit(UART_Data_to_Transmit),
	
	//Output Ports
	.SerialOutput_Tx(SerialData_Tx),
	.Rx_Interrupt(UART_Interrupt_bit),
	.Parity_Error(),
	.Received_Data(UART_Received_Data)
);	
	
/*********************************************************************************************************************************/
/**********************************************        Data Feeder        ********************************************************/
/*********************************************************************************************************************************/
DataFeeder
#(
	.Word_Length(WORD_LENGTH)
)
DATA_FEEDER
(
//Input ports
	.uartClk(uartClk2),
	.Clk_1kHz(Clk_1kHz),
	.reset(reset),
	.calculusReady(calculusReady_log),
	.push(UART_Interrupt_bit),
	.uartData(UART_Received_Data),
	
	//Output ports
	.startCalculus(startCalculus_log),
	.retransmit(retransmit_log),
	.pushFIFO1(pushFIFO1_log),
	.pushFIFO2(pushFIFO2_log),
	.pushFIFO3(pushFIFO3_log),
	.pushFIFO4(pushFIFO4_log),
	.pushFIFO5(pushFIFO5_log),
	.pushFIFO6(pushFIFO6_log),
	.pushFIFO7(pushFIFO7_log),
	.pushFIFO8(pushFIFO8_log),
	.pushVector(pushVector_log),
	.dataFIFO1(dataFIFO1_log),
	.dataFIFO2(dataFIFO2_log),
	.dataFIFO3(dataFIFO3_log),
	.dataFIFO4(dataFIFO4_log),
	.dataFIFO5(dataFIFO5_log),
	.dataFIFO6(dataFIFO6_log),
	.dataFIFO7(dataFIFO7_log),
	.dataFIFO8(dataFIFO8_log),
	.dataVector(dataVector_log),
	.matrixSize(matrixSize_log)
	
);

/*********************************************************************************************************************************/
/************************************************        FIFO's        ***********************************************************/
/*********************************************************************************************************************************/
FIFO
#(
	//DATA_WIDTH: indicates the number of bits of the width
	.DATA_WIDTH(8), 
	//ADDR_WIDTH: indicates the number of bits of the address width
	.ADDR_WIDTH(3)
)
FIFO_1
(
	//Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.DataInput(dataFIFO1_log),
	.push(pushFIFO1_log),
	.pop(popSignal_1_log),

	//Output Ports
	.DataOutput(FIFO1_DataOut),
	.full(),
	.empty()
);

FIFO
#(
	//DATA_WIDTH: indicates the number of bits of the width
	.DATA_WIDTH(8), 
	//ADDR_WIDTH: indicates the number of bits of the address width
	.ADDR_WIDTH(3)
)
FIFO_2
(
	//Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.DataInput(dataFIFO2_log),
	.push(pushFIFO2_log),
	.pop(popSignal_2_log),

	//Output Ports
	.DataOutput(FIFO2_DataOut),
	.full(),
	.empty()
);

FIFO
#(
	//DATA_WIDTH: indicates the number of bits of the width
	.DATA_WIDTH(8), 
	//ADDR_WIDTH: indicates the number of bits of the address width
	.ADDR_WIDTH(3)
)
FIFO_3
(
	//Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.DataInput(dataFIFO3_log),
	.push(pushFIFO3_log),
	.pop(popSignal_3_log),

	//Output Ports
	.DataOutput(FIFO3_DataOut),
	.full(),
	.empty()
);

FIFO
#(
	//DATA_WIDTH: indicates the number of bits of the width
	.DATA_WIDTH(8), 
	//ADDR_WIDTH: indicates the number of bits of the address width
	.ADDR_WIDTH(3)
)
FIFO_4
(
	//Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.DataInput(dataFIFO4_log),
	.push(pushFIFO4_log),
	.pop(popSignal_4_log),

	//Output Ports
	.DataOutput(FIFO4_DataOut),
	.full(),
	.empty()
);

FIFO
#(
	//DATA_WIDTH: indicates the number of bits of the width
	.DATA_WIDTH(8), 
	//ADDR_WIDTH: indicates the number of bits of the address width
	.ADDR_WIDTH(3)
)
FIFO_5
(
	//Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.DataInput(dataFIFO5_log),
	.push(pushFIFO5_log),
	.pop(popSignal_5_log),

	//Output Ports
	.DataOutput(FIFO5_DataOut),
	.full(),
	.empty()
);

FIFO
#(
	//DATA_WIDTH: indicates the number of bits of the width
	.DATA_WIDTH(8), 
	//ADDR_WIDTH: indicates the number of bits of the address width
	.ADDR_WIDTH(3)
)
FIFO_6
(
	//Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.DataInput(dataFIFO6_log),
	.push(pushFIFO6_log),
	.pop(popSignal_6_log),

	//Output Ports
	.DataOutput(FIFO6_DataOut),
	.full(),
	.empty()
);

FIFO
#(
	//DATA_WIDTH: indicates the number of bits of the width
	.DATA_WIDTH(8), 
	//ADDR_WIDTH: indicates the number of bits of the address width
	.ADDR_WIDTH(3)
)
FIFO_7
(
	//Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.DataInput(dataFIFO7_log),
	.push(pushFIFO7_log),
	.pop(popSignal_7_log),

	//Output Ports
	.DataOutput(FIFO7_DataOut),
	.full(),
	.empty()
);

FIFO
#(
	//DATA_WIDTH: indicates the number of bits of the width
	.DATA_WIDTH(8), 
	//ADDR_WIDTH: indicates the number of bits of the address width
	.ADDR_WIDTH(3)
)
FIFO_8
(
	//Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.DataInput(dataFIFO8_log),
	.push(pushFIFO8_log),
	.pop(popSignal_8_log),

	//Output Ports
	.DataOutput(FIFO8_DataOut),
	.full(),
	.empty()
);

Demultiplexer2to1
#(
	.NBits(1)
)
DEMUX_POP_VECTORS
(
	//Inputs
	.Selector(selector_between_1_and_5_log),
	.DEMUX_Input(vector_pop_log),
	
	//Outputs
	.DEMUX_Data0(vector1_pop_log),
	.DEMUX_Data1(vector2_pop_log)
	
);

FIFO
#(
	//DATA_WIDTH: indicates the number of bits of the width
	.DATA_WIDTH(8), 
	//ADDR_WIDTH: indicates the number of bits of the address width
	.ADDR_WIDTH(3)
)
FIFO_VECTOR
(
	//Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.DataInput(dataVector_log),
	.push(pushVector_log),
	.pop(vector1_pop_log),

	//Output Ports
	.DataOutput(Vector1_DataOut),
	.full(),
	.empty()
);

FIFO
#(
	//DATA_WIDTH: indicates the number of bits of the width
	.DATA_WIDTH(8), 
	//ADDR_WIDTH: indicates the number of bits of the address width
	.ADDR_WIDTH(3)
)
FIFO_VECTOR_EXTRA
(
	//Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.DataInput(dataVector_log),
	.push(pushVector_log),
	.pop(vector2_pop_log),

	//Output Ports
	.DataOutput(Vector2_DataOut),
	.full(),
	.empty()
);

Multiplexer2to1
#(
	.NBits(WORD_LENGTH)
)
MUX_DATA_VECTOR
(
	.Selector(selector_between_1_and_5_log),
	.MUX_Data0(Vector1_DataOut),
	.MUX_Data1(Vector2_DataOut),
	
	.MUX_Output(Vector_DataOut)

);

processorsArray
#(

	.WORD_LENGTH(8),
	.MATRIZ_LENGTH(4),
	.TOTAL_COUNTER_LENGTH(5)
)
PROCESSORSARRAY
(
	//inputs
	.clk(Clk_1kHz),
	.reset(reset),
	.vector_input(Vector_DataOut),
	//FIFO variables
	.FIFO_1_input(FIFO1_DataOut),
	.FIFO_2_input(FIFO2_DataOut),
	.FIFO_3_input(FIFO3_DataOut),
	.FIFO_4_input(FIFO4_DataOut),
	.FIFO_5_input(FIFO5_DataOut),
	.FIFO_6_input(FIFO6_DataOut),
	.FIFO_7_input(FIFO7_DataOut),
	.FIFO_8_input(FIFO8_DataOut),
	//control variables
	.start_control(startCalculus_log),
	.matriz_size(matrixSize_log),
	.retransmit_flag(),
	//outputs
	//MUX outputs
	.selector_between_1_and_5_FIFO(selector_between_1_and_5_log),
	.selector_between_2_and_6_FIFO(selector_between_2_and_6_log),
	.selector_between_3_and_7_FIFO(selector_between_3_and_7_log),
	.selector_between_4_and_8_FIFO(selector_between_4_and_8_log),
	//POPS
	.fifo_1_5_pop(popSignal_1_5_log),
	.fifo_2_6_pop(popSignal_2_6_log),
	.fifo_3_7_pop(popSignal_3_7_log),
	.fifo_4_8_pop(popSignal_4_8_log),
	.vector_pop(vector_pop_log),
	.ready(calculusReady_log),
		//processors out
	.first_processor_output(first_processor_output_wire),
	.second_processor_output(second_processor_output_wire),
	.third_processor_output(third_processor_output_wire),
	.fourth_processor_output(fourth_processor_output_wire),
		//Flags for data colector registers
	.first_processor_end_flag(register_data1and5_enable_wire),
	.second_processor_end_flag(register_data2and6_enable_wire),
	.third_processor_end_flag(register_data3and7_enable_wire),
	.fourth_processor_end_flag(register_data4and8_enable_wire)

);

Demultiplexer2to1
#(

	.NBits(1)
)
DEMUX_FIFO_1_5
(
	//Inputs
	.Selector(selector_between_1_and_5_log),
	.DEMUX_Input(popSignal_1_5_log),
	
	//Outputs
	.DEMUX_Data0(popSignal_1_log),
	.DEMUX_Data1(popSignal_5_log)
	
);

Demultiplexer2to1
#(

	.NBits(1)
)
DEMUX_FIFO_2_6
(
	//Inputs
	.Selector(selector_between_2_and_6_log),
	.DEMUX_Input(popSignal_2_6_log),
	
	//Outputs
	.DEMUX_Data0(popSignal_2_log),
	.DEMUX_Data1(popSignal_6_log)
	
);

Demultiplexer2to1
#(

	.NBits(1)
)
DEMUX_FIFO_3_7
(
	//Inputs
	.Selector(selector_between_3_and_7_log),
	.DEMUX_Input(popSignal_3_7_log),
	
	//Outputs
	.DEMUX_Data0(popSignal_3_log),
	.DEMUX_Data1(popSignal_7_log)
	
);

Demultiplexer2to1
#(

	.NBits(1)
)
DEMUX_FIFO_4_8
(
	//Inputs
	.Selector(selector_between_4_and_8_log),
	.DEMUX_Input(popSignal_4_8_log),
	
	//Outputs
	.DEMUX_Data0(popSignal_4_log),
	.DEMUX_Data1(popSignal_8_log)
	
);

Data_colector
#(
	.DATA_LENGTH(8)
)
DATA_COLECTOR
(
	//Inputs
	.processor1_data(first_processor_output_wire),
	.processor2_data(second_processor_output_wire),
	.processor3_data(third_processor_output_wire),
	.processor4_data(fourth_processor_output_wire),
	.data_selector_processor1(selector_between_1_and_5_log),
	.data_selector_processor2(selector_between_2_and_6_log),
	.data_selector_processor3(selector_between_3_and_7_log),
	.data_selector_processor4(selector_between_4_and_8_log),
	.retransmit(),
	.clk(Clk_1kHz),
	.reset(reset),
	.register_data1and5_enable(register_data1and5_enable_wire),
	.register_data2and6_enable(register_data2and6_enable_wire),
	.register_data3and7_enable(register_data3and7_enable_wire),
	.register_data4and8_enable(register_data4and8_enable_wire),
	//Outputs
	.register1_out(register1_out_wire_DC),
	.register2_out(register2_out_wire_DC),
	.register3_out(register3_out_wire_DC),
	.register4_out(register4_out_wire_DC),
	.register5_out(register5_out_wire_DC),
	.register6_out(register6_out_wire_DC),
	.register7_out(register7_out_wire_DC),
	.register8_out(register8_out_wire_DC)
	
);

Flip_Flop_T
ENABLE_COUNTER_DATA_SEND
(
	// Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.t(calculusReady_log || counter_flag_DS_wire),
	
	// Output Ports
	.q(counter_send_enable_wire)
);

OneShot
ONE_SHOT_POP_GENERALFIFO
(
	//Input Ports
	.clk(uartClk),
	.reset(reset),
	.Trigger(counter_send_enable_wire & Clk_1kHz),
	
	//Output Ports
	.Pulse(oneShot_transmit_signal_wire)
);

CounterDataSend
#(
	// NBITS FOR COUNTER: Number of bits needed to represent the counter number.
	.NBITS_FOR_COUNTER(8)
)
COUNTER_FOR_DATA_TO_SEND
(
	// Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.syncReset(),
	.enable(counter_send_enable_wire),
	.COUNT_NUM(11),
	
	// Output Ports
	.flag(counter_flag_DS_wire),
	.count(counter_DS_out_wire)
);


Mux10to1
#(
	.Word_Length(8)
)
MULTIPLEX_DATA_SEND
(
	// Input Ports
	.Selector(counter_DS_out_wire),
	.Data_0(0),
	.Data_1(8'hFE),
	.Data_2(register1_out_wire_DC),
	.Data_3(register2_out_wire_DC),
	.Data_4(register3_out_wire_DC),
	.Data_5(register4_out_wire_DC),
	.Data_6(register5_out_wire_DC),
	.Data_7(register6_out_wire_DC),
	.Data_8(register7_out_wire_DC),
	.Data_9(register8_out_wire_DC),
	.Data_10(8'hEF),
	
	// Output Ports
	.Mux_Output(UART_Data_to_Transmit)
);

endmodule