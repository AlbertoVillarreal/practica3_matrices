module Counter_RorW
#(
	// Parameter Declarations
	// MAXIMUM_VALUE: Number to count.
	parameter MAXIMUM_VALUE = 5,
	// NBITS FOR COUNTER: Number of bits needed to represent the counter number.
	parameter NBITS_FOR_COUNTER = CeilLog2(MAXIMUM_VALUE)
)

(
	// Input Ports
	input clk,
	input reset,
	input empty_full,
	input enable,
	
	// Output Ports
	output logic [NBITS_FOR_COUNTER-1:0] counter
);


/* Counting Function, it will be counting as long as the reset and enable signals are set */
always_ff@(posedge clk or negedge reset) 
begin
		if (reset == 1'b0)
			counter <= {NBITS_FOR_COUNTER{1'b0}};
			
		else 
		begin
			if(enable == 1'b1) //Start the process
				begin
				if(empty_full) //If is full or empty it clears the counter.
					counter <= 0;
				else
					counter <= counter + 1'b1;
					
				
				end
		end
end

//----------------------------------------------------------------------------------------------
/*--------------------------------------------------------------------*/
 /*--------------------------------------------------------------------*/
 /*--------------------------------------------------------------------*/
   
 /*Log Function*/
     function integer CeilLog2;
       input integer data;
       integer i,result;
       begin
          for(i=0; 2**i < data; i=i+1)
             result = i + 1;
          CeilLog2 = result;
       end
    endfunction

/*--------------------------------------------------------------------*/
/*--------------------------------------------------------------------*/
/*--------------------------------------------------------------------*/
endmodule 