module Data_colector
#(
	parameter DATA_LENGTH = 8
)
(
	//Inputs
	input [DATA_LENGTH - 1 : 0]	processor1_data,
	input [DATA_LENGTH - 1 : 0]	processor2_data,
	input [DATA_LENGTH - 1 : 0]	processor3_data,
	input [DATA_LENGTH - 1 : 0]	processor4_data,
	input 								data_selector_processor1,
	input 								data_selector_processor2,
	input 								data_selector_processor3,
	input 								data_selector_processor4,
	input 								retransmit,
	input 								clk,
	input									reset,
	input									register_data1and5_enable,
	input									register_data2and6_enable,
	input									register_data3and7_enable,
	input									register_data4and8_enable,
	
	//Outputs
	output [DATA_LENGTH - 1 : 0]	register1_out,
	output [DATA_LENGTH - 1 : 0]	register2_out,
	output [DATA_LENGTH - 1 : 0]	register3_out,
	output [DATA_LENGTH - 1 : 0]	register4_out,
	output [DATA_LENGTH - 1 : 0]	register5_out,
	output [DATA_LENGTH - 1 : 0]	register6_out,
	output [DATA_LENGTH - 1 : 0]	register7_out,
	output [DATA_LENGTH - 1 : 0]	register8_out
	
);
	
	//Demultiplexors wires
	logic [DATA_LENGTH - 1 : 0] data1_wire;
	logic [DATA_LENGTH - 1 : 0] data2_wire;
	logic [DATA_LENGTH - 1 : 0] data3_wire;
	logic [DATA_LENGTH - 1 : 0] data4_wire;
	logic [DATA_LENGTH - 1 : 0] data5_wire;
	logic [DATA_LENGTH - 1 : 0] data6_wire;
	logic [DATA_LENGTH - 1 : 0] data7_wire;
	logic [DATA_LENGTH - 1 : 0] data8_wire;
	
	//Demultiplexor for regsiter enables wires
	logic [DATA_LENGTH - 1 : 0] data1_enable_wire;
	logic [DATA_LENGTH - 1 : 0] data2_enable_wire;
	logic [DATA_LENGTH - 1 : 0] data3_enable_wire;
	logic [DATA_LENGTH - 1 : 0] data4_enable_wire;
	logic [DATA_LENGTH - 1 : 0] data5_enable_wire;
	logic [DATA_LENGTH - 1 : 0] data6_enable_wire;
	logic [DATA_LENGTH - 1 : 0] data7_enable_wire;
	logic [DATA_LENGTH - 1 : 0] data8_enable_wire;

	//Register wires
	logic [DATA_LENGTH - 1 : 0] register1_out_wire;
	logic [DATA_LENGTH - 1 : 0] register2_out_wire;
	logic [DATA_LENGTH - 1 : 0] register3_out_wire;
	logic [DATA_LENGTH - 1 : 0] register4_out_wire;
	logic [DATA_LENGTH - 1 : 0] register5_out_wire;
	logic [DATA_LENGTH - 1 : 0] register6_out_wire;
	logic [DATA_LENGTH - 1 : 0] register7_out_wire;
	logic [DATA_LENGTH - 1 : 0] register8_out_wire;
	
///////////////////////////////////////////////////////////////////////////////////////////
/*
*												DATA BETWEEN 1 AND 5
*/
	Demultiplexer2to1
#(
	.NBits(8)
)
DATA_1_5
(
	//Inputs
	.Selector(data_selector_processor1),
	.DEMUX_Input(processor1_data),
	
	//Outputs
	.DEMUX_Data0(data1_wire),
	.DEMUX_Data1(data5_wire)
	
);

	Demultiplexer2to1
#(
	.NBits(8)
)
DATA_ENABLE_1_5
(
	//Inputs
	.Selector(data_selector_processor1),
	.DEMUX_Input(register_data1and5_enable),
	
	//Outputs
	.DEMUX_Data0(data1_enable_wire),
	.DEMUX_Data1(data5_enable_wire)
	
);

 Register_With_Clock_Enable
#(
	.WORD_LENGTH(8)
)
REGISTER_FOR_DATA1
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.syncReset(),
	.Data_Input(data1_wire),
	.enable(data1_enable_wire), 
	
	// Output Ports
	.Data_Output(register1_out_wire)
);

 Register_With_Clock_Enable
#(
	.WORD_LENGTH(8)
)
REGISTER_FOR_DATA5
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.syncReset(),
	.Data_Input(data5_wire),
	.enable(data5_enable_wire), 
	
	// Output Ports
	.Data_Output(register5_out_wire)
);

////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////
/*
*												DATA BETWEEN 2 AND 6
*/
	Demultiplexer2to1
#(
	.NBits(8)
)
DATA_2_6
(
	//Inputs
	.Selector(data_selector_processor2),
	.DEMUX_Input(processor2_data),
	
	//Outputs
	.DEMUX_Data0(data2_wire),
	.DEMUX_Data1(data6_wire)
	
);

	Demultiplexer2to1
#(
	.NBits(8)
)
DATA_ENABLE_2_6
(
	//Inputs
	.Selector(data_selector_processor2),
	.DEMUX_Input(register_data2and6_enable),
	
	//Outputs
	.DEMUX_Data0(data2_enable_wire),
	.DEMUX_Data1(data6_enable_wire)
	
);

 Register_With_Clock_Enable
#(
	.WORD_LENGTH(8)
)
REGISTER_FOR_DATA2
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.syncReset(),
	.Data_Input(data2_wire),
	.enable(data2_enable_wire), 
	
	// Output Ports
	.Data_Output(register2_out_wire)
);

 Register_With_Clock_Enable
#(
	.WORD_LENGTH(8)
)
REGISTER_FOR_DATA6
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.syncReset(),
	.Data_Input(data6_wire),
	.enable(data6_enable_wire), 
	
	// Output Ports
	.Data_Output(register6_out_wire)
);

////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////
/*
*												DATA BETWEEN 3 AND 7
*/
	Demultiplexer2to1
#(
	.NBits(8)
)
DATA_3_7
(
	//Inputs
	.Selector(data_selector_processor3),
	.DEMUX_Input(processor3_data),
	
	//Outputs
	.DEMUX_Data0(data3_wire),
	.DEMUX_Data1(data7_wire)
	
);

	Demultiplexer2to1
#(
	.NBits(8)
)
DATA_ENABLE_3_7
(
	//Inputs
	.Selector(data_selector_processor3),
	.DEMUX_Input(register_data3and7_enable),
	
	//Outputs
	.DEMUX_Data0(data3_enable_wire),
	.DEMUX_Data1(data7_enable_wire)
	
);

 Register_With_Clock_Enable
#(
	.WORD_LENGTH(8)
)
REGISTER_FOR_DATA3
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.syncReset(),
	.Data_Input(data3_wire),
	.enable(data3_enable_wire), 
	
	// Output Ports
	.Data_Output(register3_out_wire)
);

 Register_With_Clock_Enable
#(
	.WORD_LENGTH(8)
)
REGISTER_FOR_DATA7
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.syncReset(),
	.Data_Input(data7_wire),
	.enable(data7_enable_wire), 
	
	// Output Ports
	.Data_Output(register7_out_wire)
);

////////////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////
/*
*												DATA BETWEEN 4 AND 8
*/
	Demultiplexer2to1
#(
	.NBits(8)
)
DATA_4_8
(
	//Inputs
	.Selector(data_selector_processor4),
	.DEMUX_Input(processor4_data),
	
	//Outputs
	.DEMUX_Data0(data4_wire),
	.DEMUX_Data1(data8_wire)
	
);

	Demultiplexer2to1
#(
	.NBits(8)
)
DATA_ENABLE_4_8
(
	//Inputs
	.Selector(data_selector_processor4),
	.DEMUX_Input(register_data4and8_enable),
	
	//Outputs
	.DEMUX_Data0(data4_enable_wire),
	.DEMUX_Data1(data8_enable_wire)
	
);

 Register_With_Clock_Enable
#(
	.WORD_LENGTH(8)
)
REGISTER_FOR_DATA4
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.syncReset(),
	.Data_Input(data4_wire),
	.enable(data4_enable_wire), 
	
	// Output Ports
	.Data_Output(register4_out_wire)
);

 Register_With_Clock_Enable
#(
	.WORD_LENGTH(8)
)
REGISTER_FOR_DATA8
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.syncReset(),
	.Data_Input(data8_wire),
	.enable(data8_enable_wire), 
	
	// Output Ports
	.Data_Output(register8_out_wire)
);

////////////////////////////////////////////////////////////////////////////////////////////////

assign register1_out = register1_out_wire;
assign register2_out = register2_out_wire;
assign register3_out = register3_out_wire;
assign register4_out = register4_out_wire;
assign register5_out = register5_out_wire;
assign register6_out = register6_out_wire;
assign register7_out = register7_out_wire;
assign register8_out = register8_out_wire;

endmodule