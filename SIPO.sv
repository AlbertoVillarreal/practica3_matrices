module SIPO
#(
	parameter WORD_LENGTH = 8,
	parameter WORD_LENGTH_COUNTER = 4
)
(
	//Input Ports
	input clk, 
	input reset,
	input syncReset,
	input SerialDataRx,
	input enable,
	input [WORD_LENGTH_COUNTER-1:0] counter,
	
	//Output Ports
	output logic [WORD_LENGTH-1:0] data
);

always_ff@(posedge clk, negedge reset)
begin
	if(reset == 1'b0)
		data <= 0;
	
	else if(syncReset == 1'b1)
		data <= 0;
	
	else
		if(enable == 1'b1)
			data[counter] <= SerialDataRx;
		else
			data <= data;
end

endmodule 