module ClockDivider
#(
	// FREQUENCY
	parameter FREQUENCY = 115_200,
	// REFERENCE FREQUENCY
	parameter REFERENCE_FREQUENCY = 50_000_000
)

(
	// Input Ports
	input clk_FPGA,
	input reset,
	input enable,
	
	// Output Ports
	output Clock_Signal
);


wire flag_wire;

/* Counter module */	
CounterClockDivider
#(
	.FREQUENCY(FREQUENCY),
	.REFERENCE_FREQUENCY(REFERENCE_FREQUENCY)
)
COUNTER_MODULE
(
	// Input Ports
	.clk_FPGA(clk_FPGA),
	.reset(reset),
	.enable(enable),
	
	// Output Ports
	.flag(flag_wire)
);

Flip_Flop_T
FLIP_FLOP
(
	// Input Ports
	.clk(clk_FPGA),
	.reset(reset),
	.t(flag_wire),
	
	// Output Ports
	.q(Clock_Signal)
);

endmodule