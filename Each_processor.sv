module Each_processor
#(
	parameter WORD_LENGTH = 4
)
(
	//Inputs
	input clk,
	input reset,
	input start_control,
	input signed [2 : 0]process_again,
	input finish_process_flag,
	
	//Outputs
	output bit enable_processor_counter,
	output bit flush,
	output bit fifo_pop,
	output bit enable_processor_data_out,
	output bit selector_fifo,
	output bit ready_flag
);
enum logic[2:0]{IDLE,WAIT, ON, OFF, WORK_AGAIN, FLUSH} state, next_state;

bit enable_processor_counter_wire;
bit flush_wire;
bit fifo_pop_wire;
bit selector_fifo_wire;
bit enable_processor_data_out_wire;
bit ready_flag_wire;

always_ff@(posedge clk or negedge reset)
begin
	if(reset == 1'b0)
		state <= IDLE;
	else
		state <= next_state;
end

always_comb
begin
	case(state)
		IDLE:
		begin
			if(start_control)
				next_state = WAIT;
			else
				next_state = IDLE;
		end
		WAIT:
		begin
			next_state = ON;
		end
		ON:
		begin
			if(finish_process_flag)
				next_state = OFF;
			else
				next_state = ON;
		end
		OFF:
		begin
			next_state = WORK_AGAIN;
		end
		WORK_AGAIN:
		begin
			if(process_again >= 1 || process_again == -4)
				next_state = WAIT;
			else
				next_state = FLUSH;
		end
		FLUSH:
		begin
			next_state = IDLE;
		end
	endcase
end

always_comb
begin
selector_fifo_wire = 0;
enable_processor_counter_wire = 0;
fifo_pop_wire = 0;
enable_processor_data_out_wire = 0;
flush_wire = 0;
ready_flag_wire = 0;

	case(state)
		IDLE:
		begin
		end
		WAIT:
		begin
			fifo_pop_wire = 1;
		end
		
		ON:
		begin
			enable_processor_counter_wire = 1;
			fifo_pop_wire = 1;
			
		end
		
		OFF:
		begin
			enable_processor_counter_wire = 0;
			fifo_pop_wire = 0;
			enable_processor_data_out_wire = 1;
			flush_wire = 1;
		end
		
		WORK_AGAIN:
		begin
			if(process_again >= 1 || process_again == -4)
				selector_fifo_wire = 1;
			else
				selector_fifo_wire = 0;
		end
		
		FLUSH:
		begin
			flush_wire = 1;
			selector_fifo_wire = 1;
			ready_flag_wire = 1;
		end
		
		default:
		begin
			selector_fifo_wire = 0;
			enable_processor_counter_wire = 0;
			fifo_pop_wire = 0;
			enable_processor_data_out_wire = 0;
			flush_wire = 0;
			ready_flag_wire = 0;
		end
	endcase
end

assign selector_fifo = selector_fifo_wire;
assign enable_processor_counter = enable_processor_counter_wire;
assign fifo_pop = fifo_pop_wire;
assign enable_processor_data_out = enable_processor_data_out_wire;
assign flush = flush_wire;
assign ready_flag = ready_flag_wire;
endmodule