module CounterWithInput
#(
	// NBITS FOR COUNTER: Number of bits needed to represent the counter number.
	parameter NBITS_FOR_COUNTER = 8
)

(
	// Input Ports
	input clk,
	input reset,
	input syncReset,
	input enable,
	input [NBITS_FOR_COUNTER-1:0] COUNT_NUM,
	
	// Output Ports
	output flag
);

/* This bit represents the flag output, it is set when the count reaches its objective */
bit MaxValue_Bit;

/* Counter variable */
logic [NBITS_FOR_COUNTER-1 : 0] Count_logic;

/* Counting Function, it will be counting as long as the reset and enable signals are set */
always_ff@(posedge clk or negedge reset) 
begin
	if (reset == 1'b0)
		Count_logic <= {NBITS_FOR_COUNTER{1'b0}};
		
	else if(syncReset == 1'b1)
		Count_logic <= {NBITS_FOR_COUNTER{1'b0}};
	
	else 
		begin
			if(enable == 1'b1)
				if(Count_logic == COUNT_NUM - 1)
					Count_logic <= 0;
				else
					Count_logic <= Count_logic + 1'b1;
		end
end

//--------------------------------------------------------------------------------------------
/* When the counter reaches its objective the output flag of this module is set */
always_comb
	if(Count_logic == COUNT_NUM - 1 )
		MaxValue_Bit = 1;
	else
		MaxValue_Bit = 0;
		
//---------------------------------------------------------------------------------------------
assign flag = MaxValue_Bit;
//----------------------------------------------------------------------------------------------

endmodule
