module multiplier
#(
	parameter WORD_LENGTH = 8
)
(
	//Inputs
	input [WORD_LENGTH - 1 : 0]multiplicand,
	input [WORD_LENGTH - 1 : 0]multiplier,
	
	//outputs
	output [WORD_LENGTH - 1 : 0]multiplication
);
	logic [WORD_LENGTH - 1 : 0]multiplication_wire;
always_comb
begin
	multiplication_wire = multiplicand * multiplier;
end

assign multiplication = multiplication_wire;
endmodule