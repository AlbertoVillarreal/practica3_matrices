module StateMachineTx
#(
	parameter WORD_LENGTH = 2
)
(
	// Input Ports
	input clk,
	input reset,
	input transmit,
	input flag_counter,
	
	// Output Ports,
	output bit enable_counter,
	output bit ready,
	output logic [WORD_LENGTH-1:0] mux_selector
);

/* States of the state machine*/
enum logic [2:0] {IDLE, START, TRANSMIT, PARITY, STOP, READY_FLAG} state; 

/*------------------------------------------------------------------------------------------*/
/* Assign the state in a sequential process and also according to the flag_counter 
	change or not the state*/
always_ff@(posedge clk, negedge reset) begin

	if(reset == 1'b0)
			state <= IDLE;
	else 
	 case(state)
		
			IDLE:
				if(transmit == 1'b1) /* If it is set, the machine changes the state */
					state <= START;
				else  /* If the pushbutton is 1, remains in the same state*/
					state <= IDLE;		

			START:
					state <= TRANSMIT;
					
			TRANSMIT:
				if(flag_counter == 1'b0) /* If the count is reached, chanfes the state*/
					state <= TRANSMIT;
				else	
					state <= PARITY;
					
			PARITY:
					state <= STOP;
					
			STOP:
					state <= READY_FLAG;
					
			READY_FLAG:
					state <= IDLE;
					
			default: /* Is the default state*/
					state <= IDLE;

			endcase
end//end always

/*------------------------------------------------------------------------------------------*/
/* Output assignment, combinatorial process*/
always_comb 
begin
	/* Clear all the output signals */
	enable_counter = 1'b0;
	mux_selector = 2'b11;
	ready = 1'b0;
	
	case(state)

		START:
			mux_selector = 2'b00;
			
		TRANSMIT:
			begin
			enable_counter = 1'b1; /*set the counter*/
			mux_selector = 2'b01;
			end
			
		PARITY:
			mux_selector = 2'b10;
			
		STOP:
			mux_selector = 2'b11;
			
		READY_FLAG:
			ready = 1'b1;
		
	default: /* Default case that clear all the signals */	
			begin
			enable_counter = 1'b0;
			mux_selector = 2'b11;
			ready = 1'b0;
			end

	endcase
end

endmodule
