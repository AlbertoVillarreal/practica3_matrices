module FIFO
#(
	//Parameters
	//DATA_WIDTH: indicates the number of bits of the width
	parameter DATA_WIDTH=8, 
	//ADDR_WIDTH: indicates the number of bits of the address width
	parameter ADDR_WIDTH=3,
	//MAXIMUM_VALUE: indicates the value to count
	parameter MAXIMUM_VALUE = 2**ADDR_WIDTH, 
	//NBITS_FOR_COUNTER: indicates the number of bits needed
	parameter NBITS_FOR_COUNTER = CeilLog2(MAXIMUM_VALUE)
	
)
(
	//Input Ports
	input clk,
	input reset,
	input [DATA_WIDTH-1:0] DataInput,
	input push,
	input pop,

	//Output Ports
	output [DATA_WIDTH-1:0] DataOutput,
	output full,
	output empty
);
//Wires that indicates if they are full or empty if they are set.
wire full_wire;
wire empty_wire;
//Indicates the value which the counter is in.
wire [NBITS_FOR_COUNTER-1:0] read_counter_wire;
wire [NBITS_FOR_COUNTER-1:0] write_counter_wire;

DataFE
#(
	.MAXIMUM_VALUE(MAXIMUM_VALUE+1)
)
Data_full_or_empty
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.push(push),
	.pop(pop),
	
	// Output Ports
	.full(full_wire),
	.empty(empty_wire)
);

Counter_RorW
#(
	// Parameter Declarations
	// NBITS FOR COUNTER: Number of bits needed to represent the counter number.
	.MAXIMUM_VALUE(MAXIMUM_VALUE)
)
Write_Counter
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.empty_full(full_wire),
	.enable(push),
	
	// Output Ports
	.counter(write_counter_wire) 
);

Counter_RorW
#(
	// Parameter Declarations
	// NBITS FOR COUNTER: Number of bits needed to represent the counter number.
	.MAXIMUM_VALUE(MAXIMUM_VALUE)
)
Read_Counter
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.empty_full(empty_wire),
	.enable(pop), 
	
	// Output Ports
	.counter(read_counter_wire) 
);

simple_dual_port_ram_single_clock
#(
	.ADDR_WIDTH(ADDR_WIDTH)
)
ram
(
	//Input Ports
	.data(DataInput),
	.read_addr(read_counter_wire), 
	.write_addr(write_counter_wire),
	.we(push & (~full_wire)), 
	.clk(clk),
	
	//Output Ports
	.q(DataOutput)
);

//Assign the wires to the outputs.
assign full = full_wire;
assign empty = empty_wire;


 /*Log Function*/
     function integer CeilLog2;
       input integer data;
       integer i,result;
       begin
          for(i=0; 2**i < data; i=i+1)
             result = i + 1;
          CeilLog2 = result;
       end
    endfunction

endmodule 
