module Mux4to1
#(
	parameter Word_Length = 1
)
(
	// Input Ports
	input [1:0] Selector,
	input [Word_Length-1:0] Data_0,
	input [Word_Length-1:0] Data_1,
	input [Word_Length-1:0] Data_2,
	input [Word_Length-1:0] Data_3,
	
	// Output Ports
	output logic [Word_Length-1:0] Mux_Output
);

always_comb
begin:This_Is_MUX
	
	if(Selector == 2'b00)
		Mux_Output = Data_0;
		
	else if(Selector == 2'b01)
		Mux_Output = Data_1;
	
	else if(Selector == 2'b10)
		Mux_Output = Data_2;
		
	else
		Mux_Output = Data_3;

end:This_Is_MUX


endmodule
