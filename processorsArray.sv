module processorsArray
#(

	parameter WORD_LENGTH = 8,
	parameter MATRIZ_LENGTH = 4,
	parameter TOTAL_COUNTER_LENGTH = 5
)
(
	//inputs
	input clk,
	input reset,
	input [WORD_LENGTH - 1 : 0]vector_input,
	//FIFO variables
	input [WORD_LENGTH - 1 : 0]FIFO_1_input,
	input [WORD_LENGTH - 1 : 0]FIFO_2_input,
	input [WORD_LENGTH - 1 : 0]FIFO_3_input,
	input [WORD_LENGTH - 1 : 0]FIFO_4_input,
	input [WORD_LENGTH - 1 : 0]FIFO_5_input,
	input [WORD_LENGTH - 1 : 0]FIFO_6_input,
	input [WORD_LENGTH - 1 : 0]FIFO_7_input,
	input [WORD_LENGTH - 1 : 0]FIFO_8_input,
	//control variables
	input bit 							start_control,
	input [MATRIZ_LENGTH - 1 : 0]	matriz_size,
	input bit							retransmit_flag,
	//outputs
	//MUX outputs
	output bit selector_between_1_and_5_FIFO,
	output bit selector_between_2_and_6_FIFO,
	output bit selector_between_3_and_7_FIFO,
	output bit selector_between_4_and_8_FIFO,
	//POPS
	output bit fifo_1_5_pop,
	output bit fifo_2_6_pop,
	output bit fifo_3_7_pop,
	output bit fifo_4_8_pop,
	output bit vector_pop,
	output bit ready,
	//processors out
	output [WORD_LENGTH - 1 : 0]first_processor_output,
	output [WORD_LENGTH - 1 : 0]second_processor_output,
	output [WORD_LENGTH - 1 : 0]third_processor_output,
	output [WORD_LENGTH - 1 : 0]fourth_processor_output,
	//Flags for data colector registers
	output bit	first_processor_end_flag,
	output bit	second_processor_end_flag,
	output bit	third_processor_end_flag,
	output bit	fourth_processor_end_flag
);

//FIFO multiplexors outputs wires
logic [WORD_LENGTH - 1 : 0]mux_output_between_1_or_5_to_FIFO_wire;
logic [WORD_LENGTH - 1 : 0]mux_output_between_2_or_6_to_FIFO_wire;
logic [WORD_LENGTH - 1 : 0]mux_output_between_3_or_7_to_FIFO_wire;
logic [WORD_LENGTH - 1 : 0]mux_output_between_4_or_8_to_FIFO_wire;
	
//Processors outputs wires
logic [WORD_LENGTH - 1 : 0]first_processor_output_wire;
logic [WORD_LENGTH - 1 : 0]second_processor_output_wire;
logic [WORD_LENGTH - 1 : 0]third_processor_output_wire;
logic [WORD_LENGTH - 1 : 0]fourth_processor_output_wire;

//Vector output wires
logic [WORD_LENGTH - 1 : 0]first_processor_vector_output_wire;
logic [WORD_LENGTH - 1 : 0]second_processor_vector_output_wire;
logic [WORD_LENGTH - 1 : 0]third_processor_vector_output_wire;
logic [WORD_LENGTH - 1 : 0]fourth_processor_vector_output_wire;

//Control output wires
bit ready_wire;
//Enable processors wires
bit enable_first_processor_wire;
bit enable_second_processor_wire;
bit enable_third_processor_wire;
bit enable_fourth_processor_wire;
//Flush processors wires
bit flush_first_processor_wire;
bit flush_second_processor_wire;
bit flush_third_processor_wire;
bit flush_fourth_processor_wire;
//Selector FIFO for processors wires
bit selector_first_processor_wire;
bit selector_second_processor_wire;
bit selector_third_processor_wire;
bit selector_fourth_processor_wire;
//Counter for processors wires
bit 												enable_total_counter_wire;
logic [TOTAL_COUNTER_LENGTH - 1 : 0]	total_count_cycle_wire;
bit												first_processor_counter_enable_wire;
bit												second_processor_counter_enable_wire;
bit												third_processor_counter_enable_wire;
bit												fourth_processor_counter_enable_wire;

//Counter wires
bit first_processor_off_wire;
bit second_processor_off_wire;
bit third_processor_off_wire;
bit fourth_processor_off_wire;
bit operation_finish_flag_wire;
//Flip Flop t
bit ff_enable_total_counter_wire;
bit ff_enable_first_processor_wire;
bit ff_enable_second_processor_wire;
bit ff_enable_third_processor_wire;
bit ff_enable_fourth_processor_wire;

//FIFOS pops wires
bit fifo_1_5_pop_wire;
bit fifo_2_6_pop_wire;
bit fifo_3_7_pop_wire;
bit fifo_4_8_pop_wire;

//Control muxes wires
logic [2 : 0] mux_for_process_again_first_controller_wire;
logic [2 : 0] mux_for_process_again_second_controller_wire;
logic [2 : 0] mux_for_process_again_third_controller_wire;
logic [2 : 0] mux_for_process_again_fourth_controller_wire;

//Enable 
bit enable_first_processor_data_out_wire;
bit enable_second_processor_data_out_wire;
bit enable_third_processor_data_out_wire;
bit enable_fourth_processor_data_out_wire;

//Vector registers
logic [WORD_LENGTH - 1 : 0] first_processor_vector_output_from_register_wire;
logic [WORD_LENGTH - 1 : 0] second_processor_vector_output_from_register_wire;
logic [WORD_LENGTH - 1 : 0] third_processor_vector_output_from_register_wire;
logic [WORD_LENGTH - 1 : 0] fourth_processor_vector_output_from_register_wire;

//Ready flags from each processor controller
bit flag_first_processor_wire;
bit flag_second_processor_wire;
bit flag_third_processor_wire;
bit flag_fourth_processor_wire;

//Flip flop for total value counter
Flip_Flop_T
TOTAL_VALUE_FFT
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.t(enable_total_counter_wire),
	
	// Output Ports
	.q(ff_enable_total_counter_wire)
);

//Counter for the total cycle values
CounterWithInput
#(
	// NBITS FOR COUNTER: Number of bits needed to represent the counter number.
	.NBITS_FOR_COUNTER(5)
)
TOTAL_VALUE_COUNTER
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.syncReset(),
	.enable(ff_enable_total_counter_wire),
	.COUNT_NUM(total_count_cycle_wire),
	
	// Output Ports
	.flag(operation_finish_flag_wire)
);



Processor_control
#(
	.VALUE_TO_COUNT_LENGTH(4),
	.TOTAL_VALUE_TO_COUNT(5),
	.SELECTOR_LENGTH(3)
)
CONTROL_PROCESSOR
(
	//Inputs
	.start_control(start_control),
	.clk(clk),
	.reset(reset),
	.matriz_size(matriz_size),
	//Outpus
	//Enable processors outputs
	.enable_first_processor(enable_first_processor_wire),
	.enable_second_processor(enable_second_processor_wire),
	.enable_third_processor(enable_third_processor_wire),
	.enable_fourth_processor(enable_fourth_processor_wire),
	//Counter outputs
	.enable_total_counter(enable_total_counter_wire),
	.total_count_cycle(total_count_cycle_wire)
	  	
);
//////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
*											FIRST_PROCESSOR
*/

//Multiplexor to decide value of "process again" wich decides if the processor runs again
Multiplexer2to1
#(
	.NBits(4)
)
FIRST_CONTROLER_MUX
(
	.Selector(ff_enable_first_processor_wire),
	.MUX_Data0(matriz_size - 4),
	.MUX_Data1(0),
	
	.MUX_Output(mux_for_process_again_first_controller_wire)

);

//Counter for the first processor
CounterWithInput
#(
	// NBITS FOR COUNTER: Number of bits needed to represent the counter number.
	.NBITS_FOR_COUNTER(4)
)
FIRST_PROCESSOR_COUNTER
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.syncReset(),
	.enable(first_processor_counter_enable_wire),
	.COUNT_NUM(matriz_size),
	
	// Output Ports
	.flag(first_processor_off_wire)
);

//Control for processor 1
Each_processor
#(
	.WORD_LENGTH(4)
)
FIRST_PROCESSOR_CONTROLER
(
	//Inputs
	.clk(clk),
	.reset(reset),
	.start_control(enable_first_processor_wire),
	.process_again(mux_for_process_again_first_controller_wire),
	.finish_process_flag(first_processor_off_wire),
	
	//Outputs
	.enable_processor_counter(first_processor_counter_enable_wire),
	.flush(flush_first_processor_wire),
	.fifo_pop(fifo_1_5_pop_wire),
	.enable_processor_data_out(enable_first_processor_data_out_wire),
	.selector_fifo(selector_first_processor_wire),
	.ready_flag(flag_first_processor_wire)
);

//Flip flop for first processor selector
Flip_Flop_T
SELECTOR_FIRST_PROCESSOR
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.t(selector_first_processor_wire),
	
	// Output Ports
	.q(ff_enable_first_processor_wire)
);

//First multiplexor to decide between first or fifth FIFO
Multiplexer2to1
#(
	.NBits(8)
)
FIRST_MULTIPLEXOR_PROCESSOR
(
	.Selector(ff_enable_first_processor_wire),
	.MUX_Data0(FIFO_1_input),
	.MUX_Data1(FIFO_5_input),
	
	.MUX_Output(mux_output_between_1_or_5_to_FIFO_wire)

);

processor
#(
	.WORD_LENGTH(8)
)
FIRST_PROCESSOR
(
	//Input Ports
	.clk(clk),
	.reset(reset),
	.syncReset(flush_first_processor_wire),
	.enable(first_processor_counter_enable_wire),
	.MUXSelect(selector_first_processor_wire),
	.vector(vector_input),
	.matriz(mux_output_between_1_or_5_to_FIFO_wire),
	.ready_to_send(first_processor_off_wire),
	
	//Output Ports
	.out_processor(first_processor_output_wire),
	.vector_out(first_processor_vector_output_wire)
);

Register_With_Clock_Enable
#(
	.WORD_LENGTH(WORD_LENGTH)
)
REGISTER_1_vector
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.enable(1),
	.syncReset(0),
	.Data_Input(first_processor_vector_output_wire),
	
	// Output Ports
	.Data_Output(first_processor_vector_output_from_register_wire)

);
//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
*											SECOND_PROCESSOR
*/

//Multiplexor to decide value of "process again" wich decides if the processor runs again
Multiplexer2to1
#(
	.NBits(4)
)
SECOND_CONTROLER_MUX
(
	.Selector(ff_enable_second_processor_wire),
	.MUX_Data0(matriz_size - 5),
	.MUX_Data1(0),
	
	.MUX_Output(mux_for_process_again_second_controller_wire)

);

//Counter for the second processor
CounterWithInput
#(
	// NBITS FOR COUNTER: Number of bits needed to represent the counter number.
	.NBITS_FOR_COUNTER(4)
)
SECOND_PROCESSOR_COUNTER
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.syncReset(),
	.enable(second_processor_counter_enable_wire),
	.COUNT_NUM(matriz_size),
	
	// Output Ports
	.flag(second_processor_off_wire)
);

//Control for processor 2
Each_processor
#(
	.WORD_LENGTH(4)
)
SECOND_PROCESSOR_CONTROLER
(
	//Inputs
	.clk(clk),
	.reset(reset),
	.start_control(enable_second_processor_wire),
	.process_again(mux_for_process_again_second_controller_wire),
	.finish_process_flag(second_processor_off_wire),
	
	//Outputs
	.enable_processor_counter(second_processor_counter_enable_wire),
	.flush(flush_second_processor_wire),
	.fifo_pop(fifo_2_6_pop_wire),
	.enable_processor_data_out(enable_second_processor_data_out_wire),
	.selector_fifo(selector_second_processor_wire),
	.ready_flag(flag_second_processor_wire)
);

//Flip flop for second processor selector
Flip_Flop_T
SELECTOR_SECOND_PROCESSOR
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.t(selector_second_processor_wire),
	
	// Output Ports
	.q(ff_enable_second_processor_wire)
);

//First multiplexor to decide between second or sixth FIFO
Multiplexer2to1
#(
	.NBits(8)
)
SECOND_MULTIPLEXOR_PROCESSOR
(
	.Selector(ff_enable_second_processor_wire),
	.MUX_Data0(FIFO_2_input),
	.MUX_Data1(FIFO_6_input),
	
	.MUX_Output(mux_output_between_2_or_6_to_FIFO_wire)

);

processor
#(
	.WORD_LENGTH(8)
)
SECOND_PROCESSOR
(
	//Input Ports
	.clk(clk),
	.reset(reset),
	.syncReset(flush_second_processor_wire),
	.enable(second_processor_counter_enable_wire),
	.MUXSelect(selector_second_processor_wire),
	.vector(first_processor_vector_output_from_register_wire),
	.matriz(mux_output_between_2_or_6_to_FIFO_wire),
	.ready_to_send(second_processor_off_wire),
	//Output Ports
	.out_processor(second_processor_output_wire),
	.vector_out(second_processor_vector_output_wire)
);

Register_With_Clock_Enable
#(
	.WORD_LENGTH(WORD_LENGTH)
)
REGISTER_2_vector
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.enable(1),
	.syncReset(0),
	.Data_Input(second_processor_vector_output_wire),
	
	// Output Ports
	.Data_Output(second_processor_vector_output_from_register_wire)

);
//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////



//////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
*											THIRD_PROCESSOR
*/

//Multiplexor to decide value of "process again" wich decides if the processor runs again
Multiplexer2to1
#(
	.NBits(4)
)
THIRD_CONTROLER_MUX
(
	.Selector(ff_enable_third_processor_wire),
	.MUX_Data0(matriz_size - 6),
	.MUX_Data1(0),
	
	.MUX_Output(mux_for_process_again_third_controller_wire)

);

//Counter for the third processor
CounterWithInput
#(
	// NBITS FOR COUNTER: Number of bits needed to represent the counter number.
	.NBITS_FOR_COUNTER(4)
)
THIRD_PROCESSOR_COUNTER
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.syncReset(),
	.enable(third_processor_counter_enable_wire),
	.COUNT_NUM(matriz_size),
	
	// Output Ports
	.flag(third_processor_off_wire)
);

//Control for processor 3
Each_processor
#(
	.WORD_LENGTH(4)
)
THIRD_PROCESSOR_CONTROLER
(
	//Inputs
	.clk(clk),
	.reset(reset),
	.start_control(enable_third_processor_wire),
	.process_again(mux_for_process_again_third_controller_wire),
	.finish_process_flag(third_processor_off_wire),
	
	//Outputs
	.enable_processor_counter(third_processor_counter_enable_wire),
	.flush(flush_third_processor_wire),
	.fifo_pop(fifo_3_7_pop_wire),
	.enable_processor_data_out(enable_third_processor_data_out_wire),
	.selector_fifo(selector_third_processor_wire),
	.ready_flag(flag_third_processor_wire)
);

//Flip flop for third processor selector
Flip_Flop_T
SELECTOR_THIRD_PROCESSOR
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.t(selector_third_processor_wire),
	
	// Output Ports
	.q(ff_enable_third_processor_wire)
);

//First multiplexor to decide between third or seventh FIFO
Multiplexer2to1
#(
	.NBits(8)
)
THIRD_MULTIPLEXOR_PROCESSOR
(
	.Selector(ff_enable_third_processor_wire),
	.MUX_Data0(FIFO_3_input),
	.MUX_Data1(FIFO_7_input),
	
	.MUX_Output(mux_output_between_3_or_7_to_FIFO_wire)

);

processor
#(
	.WORD_LENGTH(8)
)
THIRD_PROCESSOR
(
	//Input Ports
	.clk(clk),
	.reset(reset),
	.syncReset(flush_third_processor_wire),
	.enable(third_processor_counter_enable_wire),
	.MUXSelect(selector_third_processor_wire),
	.vector(second_processor_vector_output_from_register_wire),
	.matriz(mux_output_between_3_or_7_to_FIFO_wire),
	.ready_to_send(third_processor_off_wire),
	//Output Ports
	.out_processor(third_processor_output_wire),
	.vector_out(third_processor_vector_output_wire)
);

Register_With_Clock_Enable
#(
	.WORD_LENGTH(WORD_LENGTH)
)
REGISTER_3_vector
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.enable(1),
	.syncReset(0),
	.Data_Input(third_processor_vector_output_wire),
	
	// Output Ports
	.Data_Output(third_processor_vector_output_from_register_wire)

);
//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////
/*
*											FOURTH_PROCESSOR
*/

//Multiplexor to decide value of "process again" wich decides if the processor runs again
Multiplexer2to1
#(
	.NBits(4)
)
FOURTH_CONTROLER_MUX
(
	.Selector(ff_enable_fourth_processor_wire),
	.MUX_Data0(matriz_size - 7),
	.MUX_Data1(0),
	
	.MUX_Output(mux_for_process_again_fourth_controller_wire)

);

//Counter for the fourth processor
CounterWithInput
#(
	// NBITS FOR COUNTER: Number of bits needed to represent the counter number.
	.NBITS_FOR_COUNTER(4)
)
FOURTH_PROCESSOR_COUNTER
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.syncReset(),
	.enable(fourth_processor_counter_enable_wire),
	.COUNT_NUM(matriz_size),
	
	// Output Ports
	.flag(fourth_processor_off_wire)
);

//Control for processor 4
Each_processor
#(
	.WORD_LENGTH(4)
)
FOURTH_PROCESSOR_CONTROLER
(
	//Inputs
	.clk(clk),
	.reset(reset),
	.start_control(enable_fourth_processor_wire),
	.process_again(mux_for_process_again_fourth_controller_wire),
	.finish_process_flag(fourth_processor_off_wire),
	
	//Outputs
	.enable_processor_counter(fourth_processor_counter_enable_wire),
	.flush(flush_fourth_processor_wire),
	.fifo_pop(fifo_4_8_pop_wire),
	.enable_processor_data_out(enable_fourth_processor_data_out_wire),
	.selector_fifo(selector_fourth_processor_wire),
	.ready_flag(flag_fourth_processor_wire)
);

//Flip flop for fourth processor selector
Flip_Flop_T
SELECTOR_FOURTH_PROCESSOR
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.t(selector_fourth_processor_wire),
	
	// Output Ports
	.q(ff_enable_fourth_processor_wire)
);

//First multiplexor to decide between fourth or seventh FIFO
Multiplexer2to1
#(
	.NBits(8)
)
FOURTH_MULTIPLEXOR_PROCESSOR
(
	.Selector(ff_enable_fourth_processor_wire),
	.MUX_Data0(FIFO_4_input),
	.MUX_Data1(FIFO_8_input),
	
	.MUX_Output(mux_output_between_4_or_8_to_FIFO_wire)

);

processor
#(
	.WORD_LENGTH(8)
)
FOURTH_PROCESSOR
(
	//Input Ports
	.clk(clk),
	.reset(reset),
	.syncReset(flush_fourth_processor_wire),
	.enable(fourth_processor_counter_enable_wire),
	.MUXSelect(selector_fourth_processor_wire),
	.vector(third_processor_vector_output_from_register_wire),
	.matriz(mux_output_between_4_or_8_to_FIFO_wire),
	.ready_to_send(fourth_processor_off_wire),
	//Output Ports
	.out_processor(fourth_processor_output_wire),
	.vector_out(fourth_processor_vector_output_wire)
);

Register_With_Clock_Enable
#(
	.WORD_LENGTH(WORD_LENGTH)
)
REGISTER_4_vector
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.enable(1),
	.syncReset(0),
	.Data_Input(fourth_processor_vector_output_wire),
	
	// Output Ports
	.Data_Output(fourth_processor_vector_output_from_register_wire)

);
//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

//Asignaciones
assign selector_between_1_and_5_FIFO = ff_enable_first_processor_wire;
assign selector_between_2_and_6_FIFO = ff_enable_second_processor_wire;
assign selector_between_3_and_7_FIFO = ff_enable_third_processor_wire;
assign selector_between_4_and_8_FIFO = ff_enable_fourth_processor_wire;

assign fifo_1_5_pop = fifo_1_5_pop_wire;
assign fifo_2_6_pop = fifo_2_6_pop_wire;
assign fifo_3_7_pop = fifo_3_7_pop_wire;
assign fifo_4_8_pop = fifo_4_8_pop_wire;

assign vector_pop = fifo_1_5_pop_wire;

assign ready = operation_finish_flag_wire;

assign first_processor_output = first_processor_output_wire;
assign second_processor_output = second_processor_output_wire;
assign third_processor_output = third_processor_output_wire;
assign fourth_processor_output = fourth_processor_output_wire;

//Enables or data colector rregisters
assign first_processor_end_flag = first_processor_off_wire;
assign second_processor_end_flag = second_processor_off_wire;
assign third_processor_end_flag = third_processor_off_wire;
assign fourth_processor_end_flag = fourth_processor_off_wire;

endmodule