module CounterClockDivider
#(
	// Parameter Declarations
	
	// FREQUENCY: Frequency of the signal that the user wants to generate.
	parameter FREQUENCY = 115_200,
	// REFERENCE FREQUENCY: Frequency of the clk signal coming from the FPGA.
	parameter REFERENCE_FREQUENCY = 50_000_000,
	// COUNT NUM: Total times that the counter has to count to set the flag signal.
	parameter COUNT_NUM = Count(REFERENCE_FREQUENCY, FREQUENCY),
	// NBITS FOR COUNTER: Number of bits needed to represent the counter number.
	parameter NBITS_FOR_COUNTER = CeilLog2(COUNT_NUM)
)

(
	// Input Ports
	input clk_FPGA,
	input reset,
	input enable,
	
	// Output Ports
	output flag
);

/* This bit represents the flag output, it is set when the count reaches its objective */
bit MaxValue_Bit;

/* Counter variable */
logic [NBITS_FOR_COUNTER-1 : 0] Count_logic;

/* Counting Function, it will be counting as long as the reset and enable signals are set */
always_ff@(posedge clk_FPGA or negedge reset) 
begin
	if (reset == 1'b0)
		Count_logic <= {NBITS_FOR_COUNTER{1'b0}};
	
	else 
		begin
			if(enable == 1'b1)
				if(Count_logic == COUNT_NUM - 1)
					Count_logic <= 0;
				else
					Count_logic <= Count_logic + 1'b1;
		end
end

//--------------------------------------------------------------------------------------------
/* When the counter reaches its objective the output flag of this module is set */
always_comb
	if(Count_logic == COUNT_NUM - 1 )
		MaxValue_Bit = 1;
	else
		MaxValue_Bit = 0;
		
//---------------------------------------------------------------------------------------------
assign flag = MaxValue_Bit;
//----------------------------------------------------------------------------------------------

/*--------------------------------------------------------------------*/
/*--------------------------------------------------------------------*/
/*--------------------------------------------------------------------*/
  
/*Log Function*/

// This function computes the necessary bits to represent the count value.

     function integer CeilLog2;
       input integer data;
       integer i,result;
       begin
          for(i=0; 2**i < data; i=i+1)
             result = i + 1;
          CeilLog2 = result;
       end
    endfunction

/*--------------------------------------------------------------------*/
/*--------------------------------------------------------------------*/
/*--------------------------------------------------------------------*/

/*Count Function*/
 
// The total count is computed by dividing the frequency of the clk between 
// the frequency of the signal we want to generate. The result is divided 
// by 2 again to have a 50 % Duty Cycle.
		function integer Count;
			input integer reference_Frequency, generate_Frequency;
			integer result;
			begin 
			result = (reference_Frequency/generate_Frequency)/2;
			Count = result;
			end
		endfunction 
		
endmodule
