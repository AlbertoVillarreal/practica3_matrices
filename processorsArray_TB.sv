timeunit 1ps; //It specifies the time unit that all the delay will take in the simulation.
timeprecision 1ps;// It specifies the resolution in the simulation.


module processorsArray_TB;
	bit clk = 0;
	bit reset;
	logic [8 : 0] vector_input;
	logic [8 : 0] FIFO_1_input;
	logic [8 : 0] FIFO_2_input;
	logic [8 : 0] FIFO_3_input;
	logic [8 : 0] FIFO_4_input;
	logic [8 : 0] FIFO_5_input;
	logic [8 : 0] FIFO_6_input;
	logic [8 : 0] FIFO_7_input;
	logic [8 : 0] FIFO_8_input;
	
	bit start_control;
	logic [3 : 0] matriz_size = 8;
	
	logic [3 : 0]selector_between_1_and_5_FIFO;
	logic [3 : 0]selector_between_2_and_6_FIFO;
	logic [3 : 0]selector_between_3_and_7_FIFO;
	logic [3 : 0]selector_between_4_and_8_FIFO;
	
	bit fifo_1_5_pop;
	bit fifo_2_6_pop;
	bit fifo_3_7_pop;
	bit fifo_4_8_pop;
	
	
processorsArray
DUT
(
	//inputs
	.clk(clk),
	.reset(reset),
	.vector_input(vector_input),
	//FIFO variables
	.FIFO_1_input(FIFO_1_input),
	.FIFO_2_input(FIFO_2_input),
	.FIFO_3_input(FIFO_3_input),
	.FIFO_4_input(FIFO_4_input),
	.FIFO_5_input(FIFO_5_input),
	.FIFO_6_input(FIFO_6_input),
	.FIFO_7_input(FIFO_7_input),
	.FIFO_8_input(FIFO_8_input),
	//control variables
	.start_control(start_control),
	.matriz_size(matriz_size),
	.retransmit_flag(),
	//outputs
	//MUX outputs
	.selector_between_1_and_5_FIFO(selector_between_1_and_5_FIFO),
	.selector_between_2_and_6_FIFO(selector_between_2_and_6_FIFO),
	.selector_between_3_and_7_FIFO(selector_between_3_and_7FIFO),
	.selector_between_4_and_8_FIFO(selector_between_4_and_8_FIFO),
	//POPS
	.fifo_1_5_pop(fifo_1_5_pop),
	.fifo_2_6_pop(fifo_2_6_pop),
	.fifo_3_7_pop(fifo_3_7_pop),
	.fifo_4_8_pop(fifo_4_8_pop)
);

  
  initial // Clock generator
  begin
    forever #2 clk = !clk;
  end
  /*********************************************************/
initial begin // reset generator
	#0 reset = 0;
	#4	reset = 1;
	#2 start_control = 1;
	#2 start_control = 0;
	#4 vector_input = 5;
		FIFO_1_input = 4;
		FIFO_2_input = 4;
		FIFO_3_input = 4;
		FIFO_4_input = 4;
		FIFO_5_input = 4;
		FIFO_6_input = 4;
		FIFO_7_input = 4;
		FIFO_8_input = 4;
end

endmodule