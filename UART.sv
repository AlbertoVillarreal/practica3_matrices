module UART
#(
	parameter WORD_LENGTH = 8,				//Number of bits of the data
	parameter WORD_LENGTH_COUNTER = 4,  //Number of bits of the counter
	parameter WORD_LENGTH_MUX = 2			//Number of bits of the selector of the mux
)
(
	//Input Ports
	input clk_230400,
	input clk_115200,
	input reset,
	input Transmit, 
	input SerialData_Rx,
	input Clear_Interrupt,
	input [WORD_LENGTH-1:0] Data_to_Transmit,
	
	//Output Ports
	output SerialOutput_Tx,
	output Rx_Interrupt,
	output Parity_Error,
	output [WORD_LENGTH-1:0] Received_Data
);

/*Wires of the output signals of the counter of TX and RX */
wire Tx_CounterFlag_wire;
wire Tx_CounterEnable_wire;
wire Rx_CounterFlag_wire/*synthesis keep*/;
wire Rx_CounterEnable_wire/*synthesis keep*/;
/*Wire of the accumulator of the output data of the Rx*/

wire [WORD_LENGTH_COUNTER-1:0] TXCount_logic_wire/*synthesis keep*/;

wire [WORD_LENGTH-1:0] Rx_DataAcumulator_Output_wire/*synthesis keep*/;

/*Wire that indicates the number that the counter is in*/ 
wire [WORD_LENGTH_COUNTER-1:0] counter_wire;
/*Wire of the selector of the mux of RX */
wire [WORD_LENGTH_MUX-1:0] Rx_MUXSelector_wire;
/* Wires of the output signals of the state machine */
wire shift_result_wire;
wire enable_parityBit_wire;
wire Rx_Interrupt_wire;
wire syncReset_wire;
wire enableReg_wire;

wire [WORD_LENGTH-1:0] SIPO_Output_wire;

wire enable_counter_clk_wire/*synthesis keep*/;
wire flag_counter_clk_wire/*synthesis keep*/;

/******************************************** UART TX **********************************************/

StateMachineTx
MooreStateMachine_Tx
(
	.clk(clk_115200),
	.reset(reset),
	.transmit(Transmit),
	.flag_counter(Tx_CounterFlag_wire),
	
	// Output Ports,
	.enable_counter(Tx_CounterEnable_wire),
	.mux_selector(Rx_MUXSelector_wire),
	.ready()//ready_Tx
);

CounterWithFunction
Counter_Tx
(
	.clk(clk_115200),
	.reset(reset),
	.enable(Tx_CounterEnable_wire),
	
	// Output Ports
	.CountOut(counter_wire),
	.flag(Tx_CounterFlag_wire)
);


Shift
Shift_Tx
(
	// Input Ports
	.data(Data_to_Transmit),
	.counter(counter_wire),
	// Output Ports
	.result(shift_result_wire)
);

Mux4to1
MuxForTx
(
	// Input Ports
	.Selector(Rx_MUXSelector_wire),
	.Data_0(1'b0),
	.Data_1(shift_result_wire),
	.Data_2(^Data_to_Transmit),
	.Data_3(1'b1),
	
	// Output Ports
	.Mux_Output(SerialOutput_Tx)
);


/******************************************** UART RX **********************************************/
StateMachineRx
MooreStateMachine_Rx
(
	// Input Ports
	.clk(clk_230400),
	.reset(reset),
	.flag_counter(Rx_CounterFlag_wire),
	.SerialData_Rx(SerialData_Rx),
	.Clear_Interrupt(Clear_Interrupt),
	.flag_counter_clk(flag_counter_clk_wire),
	
	// Output Ports,
	.enable_counter(Rx_CounterEnable_wire),
	.enable_parityBit(enable_parityBit_wire),			//////
	.syncReset(syncReset_wire),
	.Rx_Interrupt(Rx_Interrupt),
	.enable_counter_clk(enable_counter_clk_wire),
	.enable_Reg(enableReg_wire)
);

CounterWithFunction
#(
	.MAXIMUM_VALUE(8)
)
Counter_Data_Rx
(
	.clk(clk_230400),
	.reset(reset),
	.enable(Rx_CounterEnable_wire),
	
	// Output Ports
	.CountOut(TXCount_logic_wire),				////////
	.flag(Rx_CounterFlag_wire)
);

SIPO
RX_SIPO
(
	//Input Ports
	.clk(clk_230400), 
	.reset(reset),
	.syncReset(syncReset_wire),
	.SerialDataRx(SerialData_Rx),
	.enable(Rx_CounterEnable_wire),
	.counter(TXCount_logic_wire),
	
	//Output Ports
	.data(SIPO_Output_wire)
);

CounterWithFunction
#(
	.MAXIMUM_VALUE(1)
)
Counter_Wait
(
	.clk(clk_230400),
	.reset(reset),
	.enable(enable_counter_clk_wire),
	
	// Output Ports
	.CountOut(),				
	.flag(flag_counter_clk_wire)
);

Register
Rx_ReceivedData
(
	// Input Ports
	.clk(clk_230400),
	.reset(reset),
	.syncReset(1'b0),
	.data(SIPO_Output_wire),
	.enable(enableReg_wire), 
	
	// Output Ports
	.result(Received_Data)
);

FlagRegister
ParityError_Reg
(
	// Input Ports
	.clk(clk_230400),
	.reset(reset),
	.syncReset(1'b0),
	.flag_in(),//(^Received_Data) ^ SerialData_Rx),
	.enable(enable_parityBit_wire), 
	
	// Output Ports
	.flag_out(Parity_Error)
);

endmodule 