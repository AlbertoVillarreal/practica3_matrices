module Demux1to9
#(
	// Word_Length: Number of bits needed to represent the inputs and outputs.
	parameter Word_Length = 8
)
(
	// Input Ports
	input [Word_Length-1:0]Data,
	input [3:0] Selector,
	
	// Output Ports
	output logic [Word_Length-1:0] Mux_Output_0,
	output logic [Word_Length-1:0] Mux_Output_1,
	output logic [Word_Length-1:0] Mux_Output_2,
	output logic [Word_Length-1:0] Mux_Output_3,
	output logic [Word_Length-1:0] Mux_Output_4,
	output logic [Word_Length-1:0] Mux_Output_5,
	output logic [Word_Length-1:0] Mux_Output_6,
	output logic [Word_Length-1:0] Mux_Output_7,
	output logic [Word_Length-1:0] Mux_Output_8
);

enum logic[3:0]{DATA_0_SELECTED,DATA_1_SELECTED,DATA_2_SELECTED,DATA_3_SELECTED,DATA_4_SELECTED,DATA_5_SELECTED,DATA_6_SELECTED,DATA_7_SELECTED,DATA_8_SELECTED} cases;


/* Is combinational and depending of the selector is the output data */
always_comb
begin

	Mux_Output_0 = 0;
	Mux_Output_1 = 0;
	Mux_Output_2 = 0;
	Mux_Output_3 = 0;
	Mux_Output_4 = 0;
	Mux_Output_5 = 0;
	Mux_Output_6 = 0;
	Mux_Output_7 = 0;
	Mux_Output_8 = 0;
	
	case(Selector)
		DATA_0_SELECTED: Mux_Output_0 = Data;
		DATA_1_SELECTED: Mux_Output_1 = Data;
		DATA_2_SELECTED: Mux_Output_2 = Data;
		DATA_3_SELECTED: Mux_Output_3 = Data;
		DATA_4_SELECTED: Mux_Output_4 = Data;
		DATA_5_SELECTED: Mux_Output_5 = Data;
		DATA_6_SELECTED: Mux_Output_6 = Data;
		DATA_7_SELECTED: Mux_Output_7 = Data;
		DATA_8_SELECTED: Mux_Output_8 = Data;
		default:
		begin
			Mux_Output_0 = 0;
			Mux_Output_1 = 0;
			Mux_Output_2 = 0;
			Mux_Output_3 = 0;
			Mux_Output_4 = 0;
			Mux_Output_5 = 0;
			Mux_Output_6 = 0;
			Mux_Output_7 = 0;
			Mux_Output_8 = 0;
		end
	endcase

end


endmodule
