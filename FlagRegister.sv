module FlagRegister
(
	// Input Ports
	input clk,
	input reset,
	input syncReset,
	input flag_in,
	input enable, 
	
	// Output Ports
	output logic flag_out
);

always_ff@(posedge clk, negedge reset) 
begin
	
	// Asynchronous reset signal
	if(reset == 1'b0) 
		flag_out <= 1'b0;
	
	// Synchronous reset signal
	else if(syncReset == 1'b1)
		flag_out <= 1'b0;
	
	else
		begin
			
			if(enable == 1'b1)	
				flag_out <= flag_in;
				
			else
				flag_out <= flag_out;
		end
end

endmodule 