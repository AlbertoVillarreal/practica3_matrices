module FlipFlop_D
(
	// Input Ports
	input clk,
	input reset,
	input data,
	
	// Output Ports
	output bit q
);

always_ff@(posedge clk, negedge reset)
begin
	if (~reset) 
		q <= 1'b0;
	
	else
		q <= data;
end

endmodule
