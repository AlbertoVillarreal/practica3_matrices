module Shift
#(
	parameter NUMBER_BITS = 8,
	parameter NUMBER_BITS_COUNTER = 4
)
(
	// Input Ports
	input [NUMBER_BITS-1:0] data,
	input [NUMBER_BITS_COUNTER-1:0] counter,
	
	// Output Ports
	output logic result
);


logic [NUMBER_BITS-1:0] result_logic;

always_comb
begin

	result_logic = data >> counter;  
	result = result_logic[0]; 		
	
end
endmodule 
