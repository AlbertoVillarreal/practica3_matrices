module DataFE
#(
	// Parameter Declarations
	// MAXIMUM_VALUE: Number to count.
	parameter MAXIMUM_VALUE = 5,
	// NBITS FOR COUNTER: Number of bits needed to represent the counter number.
	parameter NBITS_FOR_COUNTER = CeilLog2(MAXIMUM_VALUE)
)

(
	// Input Ports
	input clk,
	input reset,
	input push,
	input pop,
	
	// Output Ports
	output bit full,
	output bit empty
);

/* This logic represents the count */
logic [NBITS_FOR_COUNTER-1:0] Count_logic;

/* Counting Function, it will be counting as long as the reset and enable signals are set */
always_ff@(posedge clk or negedge reset) 
begin
		if (reset == 1'b0)
			Count_logic <= {NBITS_FOR_COUNTER{1'b0}};
			
		else 
		begin
			if(push == 1'b1 && pop == 1'b0) //If it is set, indicates write
				begin
				if(Count_logic == MAXIMUM_VALUE) //Check if the counter reached the count
					begin
						Count_logic <= 0; //Clear the count 
					end
						
				else
					begin
						Count_logic <= Count_logic + 1'b1;
					end
				end
				
			else if(push == 1'b0 && pop == 1'b1) //If it is set, indicates read
				begin
				if(Count_logic == 1'b0) //Check if the counter is zero
					begin 
						Count_logic <= Count_logic;
					end
				else 
					begin
						Count_logic <= Count_logic - 1'b1;
					end
				end
			
			else if(push == 1'b1 && pop == 1'b1)
				begin
					Count_logic <= Count_logic;
				end
		end
end

//--------------------------------------------------------------------------------------------
/* When the counter reaches its objective the output flag of this module is set */
always_comb
begin
	if(Count_logic == MAXIMUM_VALUE-1)
		 begin
		 full <= 1'b1; // Indicates that the memory is full
		 empty <= 1'b0;
		 end
	else if (Count_logic == 0)
		 begin
			empty <= 1'b1; //Indicates that the memory is empty
			full <= 1'b0;
		 end
	else
		begin
			empty <= 1'b0;
			full <= 1'b0;
		end
end
//----------------------------------------------------------------------------------------------
/*--------------------------------------------------------------------*/
 /*--------------------------------------------------------------------*/
 /*--------------------------------------------------------------------*/
   
 /*Log Function*/
     function integer CeilLog2;
       input integer data;
       integer i,result;
       begin
          for(i=0; 2**i < data; i=i+1)
             result = i + 1;
          CeilLog2 = result;
       end
    endfunction

/*--------------------------------------------------------------------*/
/*--------------------------------------------------------------------*/
/*--------------------------------------------------------------------*/

endmodule 