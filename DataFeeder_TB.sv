timeunit 1ps; //It specifies the time unit that all the delay will take in the simulation.
timeprecision 1ps;// It specifies the resolution in the simulation.


module DataFeeder_TB;
	//Input ports
	logic uartClk = 0;
	logic Clk_1kHz = 0;
	logic reset = 0;
	logic calculusReady = 0;
	logic push = 0;
	logic [8 - 1 : 0] uartData = 0;
	logic popSignal = 0;
	
	//Output ports
	logic startCalculus_log;
	logic retransmit_log;
	logic pushFIFO1_log;
	logic pushFIFO2_log;
	logic pushFIFO3_log;
	logic pushFIFO4_log;
	logic pushFIFO5_log;
	logic pushFIFO6_log;
	logic pushFIFO7_log;
	logic pushFIFO8_log;
	logic pushVector_log;
	logic [8 - 1 : 0] dataFIFO1_log;
	logic [8 - 1 : 0] dataFIFO2_log;
	logic [8 - 1 : 0] dataFIFO3_log;
	logic [8 - 1 : 0] dataFIFO4_log;
	logic [8 - 1 : 0] dataFIFO5_log;
	logic [8 - 1 : 0] dataFIFO6_log;
	logic [8 - 1 : 0] dataFIFO7_log;
	logic [8 - 1 : 0] dataFIFO8_log;
	logic [8 - 1 : 0] dataVector_log;
	logic [8 - 1 : 0] matrixSize_log;
	
	logic [8 - 1 : 0] FIFO1_DataOut;
	logic [8 - 1 : 0] FIFO2_DataOut;
	logic [8 - 1 : 0] FIFO3_DataOut;
	logic [8 - 1 : 0] FIFO4_DataOut;
	logic [8 - 1 : 0] FIFO5_DataOut;
	logic [8 - 1 : 0] FIFO6_DataOut;
	logic [8 - 1 : 0] FIFO7_DataOut;
	logic [8 - 1 : 0] FIFO8_DataOut;
	logic [8 - 1 : 0] Vector_DataOut;

DataFeeder
DUT
(
//Input ports
	.uartClk(uartClk),
	.Clk_1kHz(Clk_1kHz),
	.reset(reset),
	.calculusReady(calculusReady),
	.push(push),
	.uartData(uartData),
	
	//Output ports
	.startCalculus(startCalculus_log),
	.retransmit(retransmit_log),
	.pushFIFO1(pushFIFO1_log),
	.pushFIFO2(pushFIFO2_log),
	.pushFIFO3(pushFIFO3_log),
	.pushFIFO4(pushFIFO4_log),
	.pushFIFO5(pushFIFO5_log),
	.pushFIFO6(pushFIFO6_log),
	.pushFIFO7(pushFIFO7_log),
	.pushFIFO8(pushFIFO8_log),
	.pushVector(pushVector_log),
	.dataFIFO1(dataFIFO1_log),
	.dataFIFO2(dataFIFO2_log),
	.dataFIFO3(dataFIFO3_log),
	.dataFIFO4(dataFIFO4_log),
	.dataFIFO5(dataFIFO5_log),
	.dataFIFO6(dataFIFO6_log),
	.dataFIFO7(dataFIFO7_log),
	.dataFIFO8(dataFIFO8_log),
	.dataVector(dataVector_log),
	.matrixSize(matrixSize_log)
	
);

FIFO
#(
	//DATA_WIDTH: indicates the number of bits of the width
	.DATA_WIDTH(8), 
	//ADDR_WIDTH: indicates the number of bits of the address width
	.ADDR_WIDTH(3)
)
FIFO_1
(
	//Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.DataInput(dataFIFO1_log),
	.push(pushFIFO1_log),
	.pop(popSignal),

	//Output Ports
	.DataOutput(FIFO1_DataOut),
	.full(),
	.empty()
);

FIFO
#(
	//DATA_WIDTH: indicates the number of bits of the width
	.DATA_WIDTH(8), 
	//ADDR_WIDTH: indicates the number of bits of the address width
	.ADDR_WIDTH(3)
)
FIFO_2
(
	//Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.DataInput(dataFIFO2_log),
	.push(pushFIFO2_log),
	.pop(popSignal),

	//Output Ports
	.DataOutput(FIFO2_DataOut),
	.full(),
	.empty()
);

FIFO
#(
	//DATA_WIDTH: indicates the number of bits of the width
	.DATA_WIDTH(8), 
	//ADDR_WIDTH: indicates the number of bits of the address width
	.ADDR_WIDTH(3)
)
FIFO_3
(
	//Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.DataInput(dataFIFO3_log),
	.push(pushFIFO3_log),
	.pop(popSignal),

	//Output Ports
	.DataOutput(FIFO3_DataOut),
	.full(),
	.empty()
);

FIFO
#(
	//DATA_WIDTH: indicates the number of bits of the width
	.DATA_WIDTH(8), 
	//ADDR_WIDTH: indicates the number of bits of the address width
	.ADDR_WIDTH(3)
)
FIFO_4
(
	//Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.DataInput(dataFIFO4_log),
	.push(pushFIFO4_log),
	.pop(popSignal),

	//Output Ports
	.DataOutput(FIFO4_DataOut),
	.full(),
	.empty()
);

FIFO
#(
	//DATA_WIDTH: indicates the number of bits of the width
	.DATA_WIDTH(8), 
	//ADDR_WIDTH: indicates the number of bits of the address width
	.ADDR_WIDTH(3)
)
FIFO_5
(
	//Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.DataInput(dataFIFO5_log),
	.push(pushFIFO5_log),
	.pop(popSignal),

	//Output Ports
	.DataOutput(FIFO5_DataOut),
	.full(),
	.empty()
);

FIFO
#(
	//DATA_WIDTH: indicates the number of bits of the width
	.DATA_WIDTH(8), 
	//ADDR_WIDTH: indicates the number of bits of the address width
	.ADDR_WIDTH(3)
)
FIFO_6
(
	//Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.DataInput(dataFIFO6_log),
	.push(pushFIFO6_log),
	.pop(popSignal),

	//Output Ports
	.DataOutput(FIFO6_DataOut),
	.full(),
	.empty()
);

FIFO
#(
	//DATA_WIDTH: indicates the number of bits of the width
	.DATA_WIDTH(8), 
	//ADDR_WIDTH: indicates the number of bits of the address width
	.ADDR_WIDTH(3)
)
FIFO_7
(
	//Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.DataInput(dataFIFO7_log),
	.push(pushFIFO7_log),
	.pop(popSignal),

	//Output Ports
	.DataOutput(FIFO7_DataOut),
	.full(),
	.empty()
);

FIFO
#(
	//DATA_WIDTH: indicates the number of bits of the width
	.DATA_WIDTH(8), 
	//ADDR_WIDTH: indicates the number of bits of the address width
	.ADDR_WIDTH(3)
)
FIFO_8
(
	//Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.DataInput(dataFIFO8_log),
	.push(pushFIFO8_log),
	.pop(popSignal),

	//Output Ports
	.DataOutput(FIFO8_DataOut),
	.full(),
	.empty()
);

FIFO
#(
	//DATA_WIDTH: indicates the number of bits of the width
	.DATA_WIDTH(8), 
	//ADDR_WIDTH: indicates the number of bits of the address width
	.ADDR_WIDTH(3)
)
FIFO_VECTOR
(
	//Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.DataInput(dataVector_log),
	.push(pushVector_log),
	.pop(popSignal),

	//Output Ports
	.DataOutput(Vector_DataOut),
	.full(),
	.empty()
);


initial // Clock generator
  begin
    forever #2 uartClk = !uartClk;
  end
 
 
initial // Clock generator
  begin
	 forever #442 Clk_1kHz = !Clk_1kHz;
  end
  
  
  /*********************************************************/
initial begin // reset generator
	#6 reset = 1;
		/*Establecemos tamanio de matriz*/
	#2 push = 1;
	#2 uartData = 8'hFE;					
	#4 uartData = 8'h03;							//Length
	#4 uartData = 8'h01;							//Comando
	#4 uartData = 8'h08;							//8x8
	#4 uartData = 8'hEF;
		/*Comenzamos a enviar datos*/
	#4 uartData = 8'hFE;
	#4 uartData = 8'h02;							//Length
	#4 uartData = 8'h03;							//Comando
	#4 uartData = 8'hEF;
		/*Ingresamos matriz*/
	#4 uartData = 8'hFE;
	#4 uartData = 8'h1B;							//Length
	#4 uartData = 8'h04;							//Comando
	#4 uartData = 8'h00;							//Dato
	#4 uartData = 8'h01;							//Dato
	#4 uartData = 8'h02;							//Dato
	#4 uartData = 8'h03;							//Dato
	#4 uartData = 8'h04;							//Dato
	#4 uartData = 8'h05;							//Dato
	#4 uartData = 8'h06;							//Dato
	#4 uartData = 8'h07;							//Dato
	#4 uartData = 8'h08;							//Dato
	#4 uartData = 8'h09;							//Dato
	#4 uartData = 8'h0A;							//Dato
	#4 uartData = 8'h0B;							//Dato
	#4 uartData = 8'h0C;							//Dato
	#4 uartData = 8'h0D;							//Dato
	#4 uartData = 8'h0E;							//Dato
	#4 uartData = 8'h0F;							//Dato
	#4 uartData = 8'h10;							//Dato //Aqui empieza una de 5x5
	#4 uartData = 8'h11;							//Dato
	#4 uartData = 8'h12;							//Dato
	#4 uartData = 8'h13;							//Dato
	#4 uartData = 8'h14;							//Dato
	#4 uartData = 8'h15;							//Dato
	#4 uartData = 8'h16;							//Dato
	#4 uartData = 8'h17;							//Dato
	#4 uartData = 8'h18;							//Dato
	#4 uartData = 8'h19;							//Dato //Aqui empieza una de 6x6
	#4 uartData = 8'h1A;							//Dato
	#4 uartData = 8'h1B;							//Dato
	#4 uartData = 8'h1C;							//Dato
	#4 uartData = 8'h1D;							//Dato
	#4 uartData = 8'h1E;							//Dato
	#4 uartData = 8'h1F;							//Dato
	#4 uartData = 8'h20;							//Dato
	#4 uartData = 8'h21;							//Dato
	#4 uartData = 8'h22;							//Dato
	#4 uartData = 8'h23;							//Dato
	#4 uartData = 8'h24;							//Dato //Aqui empieza una de 7x7
	#4 uartData = 8'h25;							//Dato
	#4 uartData = 8'h26;							//Dato
	#4 uartData = 8'h27;							//Dato
	#4 uartData = 8'h28;							//Dato
	#4 uartData = 8'h29;							//Dato
	#4 uartData = 8'h2A;							//Dato
	#4 uartData = 8'h2B;							//Dato
	#4 uartData = 8'h2C;							//Dato
	#4 uartData = 8'h2D;							//Dato
	#4 uartData = 8'h2E;							//Dato
	#4 uartData = 8'h2F;							//Dato
	#4 uartData = 8'h30;							//Dato
	#4 uartData = 8'h31;							//Dato //Aqui empieza una de 8x8
	#4 uartData = 8'h32;							//Dato
	#4 uartData = 8'h33;							//Dato
	#4 uartData = 8'h34;							//Dato
	#4 uartData = 8'h35;							//Dato
	#4 uartData = 8'h36;							//Dato
	#4 uartData = 8'h37;							//Dato
	#4 uartData = 8'h38;							//Dato
	#4 uartData = 8'h39;							//Dato
	#4 uartData = 8'h3A;							//Dato
	#4 uartData = 8'h3B;							//Dato
	#4 uartData = 8'h3C;							//Dato
	#4 uartData = 8'h3D;							//Dato
	#4 uartData = 8'h3E;							//Dato
	#4 uartData = 8'h3F;							//Dato
	#4 uartData = 8'hEF;
		/*Ingresamos vector*/
	#4 uartData = 8'hFE;
	#4 uartData = 8'h06;							//Length
	#4 uartData = 8'h04;							//Comando
	#4 uartData = 8'h01;							//Dato
	#4 uartData = 8'h02;							//Dato
	#4 uartData = 8'h03;							//Dato
	#4 uartData = 8'h04;							//Dato
	#4 uartData = 8'h05;							//Dato
	#4 uartData = 8'h06;							//Dato
	#4 uartData = 8'h07;							//Dato
	#4 uartData = 8'h08;							//Dato
	#4 uartData = 8'hEF;
	#1 push = 0;
	
	#4000 popSignal = 1;
end

endmodule