module Mux10to1
#(
	parameter Word_Length = 8
)
(
	// Input Ports
	input [3:0] Selector,
	input [Word_Length-1:0] Data_0,
	input [Word_Length-1:0] Data_1,
	input [Word_Length-1:0] Data_2,
	input [Word_Length-1:0] Data_3,
	input [Word_Length-1:0] Data_4,
	input [Word_Length-1:0] Data_5,
	input [Word_Length-1:0] Data_6,
	input [Word_Length-1:0] Data_7,
	input [Word_Length-1:0] Data_8,
	input [Word_Length-1:0] Data_9,
	input	[Word_Length-1:0] Data_10,
	
	// Output Ports
	output logic [Word_Length-1:0] Mux_Output
);

always_comb
begin:This_Is_MUX
	
	if(Selector == 4'b0000)
		Mux_Output = Data_0;
		
	else if(Selector == 4'b0001)
		Mux_Output = Data_1;
	
	else if(Selector == 4'b0010)
		Mux_Output = Data_2;
		
	else if(Selector == 4'b0011)
		Mux_Output = Data_3;
		
	else if(Selector == 4'b0100)
		Mux_Output = Data_4;

	else if(Selector == 4'b0101)
		Mux_Output = Data_5;
		
	else if(Selector == 4'b0110)
		Mux_Output = Data_6;
		
	else if(Selector == 4'b0111)
		Mux_Output = Data_7;
		
	else if(Selector == 4'b1000)
		Mux_Output = Data_8;
		
	else if(Selector == 4'b1001)
		Mux_Output = Data_9;
		
	else if(Selector == 4'b1010)
		Mux_Output = Data_10;
	else 
		Mux_Output = 0;

end:This_Is_MUX


endmodule
