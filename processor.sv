module processor
#(
	parameter WORD_LENGTH = 8
)
(
	//Input Ports
	input clk,
	input reset,
	input syncReset,
	input	enable,
	input MUXSelect,
	input [WORD_LENGTH-1:0] vector,
	input [WORD_LENGTH-1:0] matriz,
	input bit ready_to_send,
	//Output Ports

	output [WORD_LENGTH-1:0] out_processor,
	output [WORD_LENGTH-1:0] vector_out
);

bit enable_Reg1;
bit enable_Reg2;
logic [WORD_LENGTH - 1 : 0] accumulator_wire;
logic [WORD_LENGTH - 1 : 0] second_accumulator_wire;
logic [WORD_LENGTH - 1 : 0] mux_accumulator_wire;
logic [WORD_LENGTH - 1 : 0] multiplication_wire;
logic [WORD_LENGTH - 1 : 0] adder_wire;

logic [WORD_LENGTH - 1 : 0] data_out_wire;

Demultiplexer2to1
#(

	.NBits(1)
)
DEMUX_ENABLE
(
	//Inputs
	.Selector(MUXSelect),
	.DEMUX_Input(enable),
	
	//Outputs
	.DEMUX_Data0(enable_Reg1),
	.DEMUX_Data1(enable_Reg2)
	
);

Register_With_Clock_Enable
#(
	.WORD_LENGTH(WORD_LENGTH)
)
REGISTER_1_ACC
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.enable(enable_Reg1),
	.syncReset(syncReset),
	.Data_Input(adder_wire),
	
	// Output Ports
	.Data_Output(accumulator_wire)

);

Register_With_Clock_Enable
#(
	.WORD_LENGTH(WORD_LENGTH)
)
REGISTER_2_ACC
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.enable(enable_Reg2),
	.syncReset(syncReset),
	.Data_Input(adder_wire),
	
	// Output Ports
	.Data_Output(second_accumulator_wire)

);

Multiplexer2to1
#(
	.NBits(8)
)
MUX_ACC
(
	// Input Ports
	.Selector(MUXSelect),
	.MUX_Data0(accumulator_wire),
	.MUX_Data1(second_accumulator_wire),
	
	// Output Ports
	.MUX_Output(mux_accumulator_wire)

);


multiplier
#(
	.WORD_LENGTH(WORD_LENGTH)
)
MULTIPLICATION_MODULE
(
	//Input Ports
	.multiplicand(vector),
	.multiplier(matriz),
	//Output Ports
	.multiplication(multiplication_wire)
);

adder
#(
	.WORD_LENGTH(WORD_LENGTH)
)
ADDER_MODULE
(	
	// Input Ports
	.data_1(multiplication_wire),
	.data_2(mux_accumulator_wire),
	
	// Output Ports
	.data_output(adder_wire) 

);

Multiplexer2to1
#(
	.NBits(8)
)
MULTIPLEXOR_DATAOUT
(
	.Selector(ready_to_send),
	.MUX_Data0(0),
	.MUX_Data1(adder_wire),
	
	.MUX_Output(data_out_wire)

);

assign out_processor = data_out_wire;
assign vector_out = vector;
endmodule 