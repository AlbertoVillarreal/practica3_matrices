module StateMachineRx
#(
	parameter WORD_LENGTH = 2
)
(
	// Input Ports
	input clk,
	input reset,
	input flag_counter,
	input SerialData_Rx,
	input Clear_Interrupt,
	input flag_counter_clk,
	
	// Output Ports,
	output bit enable_counter,
	output bit enable_parityBit,
	output bit Rx_Interrupt,
	output bit syncReset,
	output bit enable_counter_clk,
	output bit enable_Reg
);

/* States of the state machine*/
enum logic [4:0] {IDLE, IDLE_2, WAIT, RECEIVE, WAIT_1, PARITY, CLEAR} state; 

/*------------------------------------------------------------------------------------------*/
/* Assign the state in a sequential process and also according to the flag_counter 
	change or not the state*/
always_ff@(posedge clk, negedge reset) begin

	if(reset == 1'b0)
			state <= IDLE;
	else 
	 case(state)
		
			IDLE:
				if(SerialData_Rx == 1'b0) /* If it is set, the machine changes the state */
					state <= IDLE_2;
				else  /* If the pushbutton is 1, remains in the same state*/
					state <= IDLE;	
					
			IDLE_2:
				state <= RECEIVE;
						
			WAIT:
				if(flag_counter_clk == 1'b0)
					state <= WAIT;
				else
					state <= RECEIVE;
			
			RECEIVE:
				if(flag_counter == 1'b0) /* If the count is reached, changes the state */
					state <= WAIT;
				else	
					state <= WAIT_1;
			
			WAIT_1:
				if(flag_counter_clk == 1'b0)
					state <= WAIT_1;
				else
					state <= PARITY;
					
			PARITY:
					state <= CLEAR;
			
			CLEAR:
				if(Clear_Interrupt) /* Changes the state when is pushed the button */
					state <= IDLE;
				else
					state <= CLEAR;
					
			default: /* Is the default state*/
					state <= IDLE;

			endcase
end//end always

/*------------------------------------------------------------------------------------------*/
/* Output assignment, combinatorial process*/
always_comb 
begin
	/* Clear all the output signals */
	enable_counter = 1'b0;
	enable_parityBit = 1'b0;
	syncReset = 1'b0;
	Rx_Interrupt = 1'b0;
	enable_counter_clk = 1'b0;
	enable_Reg = 1'b0;
	
	case(state)
			
		WAIT:
			enable_counter_clk = 1'b1;
			
		RECEIVE:
			enable_counter = 1'b1; /* Sets to start the count */
		
		WAIT_1:
			enable_counter_clk = 1'b1;
			
		PARITY:
			begin
				enable_Reg = 1'b1;
				enable_parityBit = 1'b1;
			end
		
		CLEAR:
			begin
				syncReset = 1'b1;
				Rx_Interrupt = 1'b1;
			end
		
	default: /* Default case that clear all the signals */	
			begin
				enable_counter = 1'b0;
				enable_parityBit = 1'b0;
				syncReset = 1'b0;
				Rx_Interrupt = 1'b0;
				enable_counter_clk = 1'b0;
				enable_Reg = 1'b0;
			end

	endcase
end

endmodule
