module Flip_Flop_T
(
	// Input Ports
	input clk,
	input reset,
	input t,
	
	// Output Ports
	output bit q
);

/* The output signal is toggled every time the input t is in a high state */
always_ff@ (posedge clk or negedge reset)
begin
	if (reset == 1'b0) 
		q <= 1'b0;
	
	else if (t == 1'b1) 
		q <= ~q;
end

endmodule //End Of Module