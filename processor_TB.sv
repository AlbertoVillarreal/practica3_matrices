timeunit 1ps; //It specifies the time unit that all the delay will take in the simulation.
timeprecision 1ps;// It specifies the resolution in the simulation.


module processor_TB;

	//Input Ports
	logic clk = 0;
	logic reset = 0;
	logic syncReset = 0;
	logic	enable = 0;
	logic MUXSelect = 0;
	logic [8 - 1 : 0] vector = 0;
	logic [8 - 1 : 0] matriz = 0;
	
	//Output Ports
	logic [8 - 1 : 0] out_processor_log;
	
processor	
DUT
(
	//Input Ports
	.clk(clk),
	.reset(reset),
	.syncReset(syncReset),
	.enable(enable),
	.MUXSelect(MUXSelect),
	.vector(vector),
	.matriz(matriz),
	
	//Output Ports
	.out_processor(out_processor_log)
	
);


initial // Clock generator
  begin
    forever #2 clk = !clk;
  end
  
  
  
  /*********************************************************/
initial begin // reset generator
	#4 reset = 1;
	
	#8  vector = 1;
	#0  matriz = 0;
	#0  enable = 1;
	
	#4  vector = 2;
	#0  matriz = 1;
	
	#4  vector = 3;
	#0  matriz = 2;
	
	#4  vector = 4;
	#0  matriz = 3;
	#0  enable = 0;
	
end

endmodule