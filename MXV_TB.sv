timeunit 1ps; //It specifies the time unit that all the delay will take in the simulation.
timeprecision 1ps;// It specifies the resolution in the simulation.


module MXV_TB;
	//Input ports
	logic uartClk2 = 0;
	logic uartClk = 0;
	logic Clk_1kHz = 0;
	logic reset = 0;
	logic uartData = 1;
	
	//Output ports
	logic DataTransfer;
	

MXV
DUT
(
	//Input Ports
	.uartClk2(uartClk2),
	.uartClk(uartClk),
	.Clk_1kHz(Clk_1kHz),
	.reset(reset),
	.SerialData_Rx(uartData),
	
	//Output Ports
	.SerialData_Tx(DataTransfer)
);


initial // Clock generator
  begin
    forever #1 uartClk2 = !uartClk2;
  end
 

initial // Clock generator
  begin
    forever #2 uartClk = !uartClk;
  end
 
 
initial // Clock generator
  begin
	 forever #222 Clk_1kHz = !Clk_1kHz;
  end
  
  
  /*********************************************************/
initial begin // reset generator
	#6 reset = 1;
		/*Establecemos tamanio de matriz*/
	#2 uartData = 8'hFE;					
	#4 uartData = 8'h03;							//Length
	#4 uartData = 8'h01;							//Comando
	#4 uartData = 8'h08;							//8x8
	#4 uartData = 8'hEF;
		/*Comenzamos a enviar datos*/
	#4 uartData = 8'hFE;
	#4 uartData = 8'h02;							//Length
	#4 uartData = 8'h03;							//Comando
	#4 uartData = 8'hEF;
		/*Ingresamos matriz*/
	#4 uartData = 8'hFE;
	#4 uartData = 8'h1B;							//Length
	#4 uartData = 8'h04;							//Comando
	#4 uartData = 8'h00;							//Dato//Aqui empieza de 1x1
	#4 uartData = 8'h01;							//Dato
	#4 uartData = 8'h02;							//Dato
	#4 uartData = 8'h03;							//Dato//Aqui empieza de 2x2
	#4 uartData = 8'h04;							//Dato
	#4 uartData = 8'h05;							//Dato
	#4 uartData = 8'h06;							//Dato//Aqui empieza de 1x1
	#4 uartData = 8'h07;							//Dato
	#4 uartData = 8'h00;							//Dato//Aqui empieza de 1x1
	#4 uartData = 8'h01;							//Dato
	#4 uartData = 8'h02;							//Dato
	#4 uartData = 8'h03;							//Dato//Aqui empieza de 2x2
	#4 uartData = 8'h04;							//Dato
	#4 uartData = 8'h05;							//Dato
	#4 uartData = 8'h06;							//Dato//Aqui empieza de 1x1
	#4 uartData = 8'h07;							//Dato
	#4 uartData = 8'h00;							//Dato//Aqui empieza de 1x1
	#4 uartData = 8'h01;							//Dato
	#4 uartData = 8'h02;							//Dato
	#4 uartData = 8'h03;							//Dato//Aqui empieza de 2x2
	#4 uartData = 8'h04;							//Dato
	#4 uartData = 8'h05;							//Dato
	#4 uartData = 8'h06;							//Dato//Aqui empieza de 1x1
	#4 uartData = 8'h07;							//Dato
	#4 uartData = 8'h00;							//Dato//Aqui empieza de 1x1
	#4 uartData = 8'h01;							//Dato
	#4 uartData = 8'h02;							//Dato
	#4 uartData = 8'h03;							//Dato//Aqui empieza de 2x2
	#4 uartData = 8'h04;							//Dato
	#4 uartData = 8'h05;							//Dato
	#4 uartData = 8'h06;							//Dato//Aqui empieza de 1x1
	#4 uartData = 8'h07;							//Dato
	#4 uartData = 8'h00;							//Dato//Aqui empieza de 1x1
	#4 uartData = 8'h01;							//Dato
	#4 uartData = 8'h02;							//Dato
	#4 uartData = 8'h03;							//Dato//Aqui empieza de 2x2
	#4 uartData = 8'h04;							//Dato
	#4 uartData = 8'h05;							//Dato
	#4 uartData = 8'h06;							//Dato//Aqui empieza de 1x1
	#4 uartData = 8'h07;							//Dato
	#4 uartData = 8'h00;							//Dato//Aqui empieza de 1x1
	#4 uartData = 8'h01;							//Dato
	#4 uartData = 8'h02;							//Dato
	#4 uartData = 8'h03;							//Dato//Aqui empieza de 2x2
	#4 uartData = 8'h04;							//Dato
	#4 uartData = 8'h05;							//Dato
	#4 uartData = 8'h06;							//Dato//Aqui empieza de 1x1
	#4 uartData = 8'h07;							//Dato
	#4 uartData = 8'h00;							//Dato//Aqui empieza de 1x1
	#4 uartData = 8'h01;							//Dato
	#4 uartData = 8'h02;							//Dato
	#4 uartData = 8'h03;							//Dato//Aqui empieza de 2x2
	#4 uartData = 8'h04;							//Dato
	#4 uartData = 8'h05;							//Dato
	#4 uartData = 8'h06;							//Dato//Aqui empieza de 1x1
	#4 uartData = 8'h07;							//Dato
		#4 uartData = 8'h00;							//Dato//Aqui empieza de 1x1
	#4 uartData = 8'h01;							//Dato
	#4 uartData = 8'h02;							//Dato
	#4 uartData = 8'h03;							//Dato//Aqui empieza de 2x2
	#4 uartData = 8'h04;							//Dato
	#4 uartData = 8'h05;							//Dato
	#4 uartData = 8'h06;							//Dato//Aqui empieza de 1x1
	#4 uartData = 8'h07;							//Dato
	#4 uartData = 8'hEF;
		/*Ingresamos vector*/
	#4 uartData = 8'hFE;
	#4 uartData = 8'h06;							//Length
	#4 uartData = 8'h04;							//Comando
	#4 uartData = 8'h01;							//Dato
	#4 uartData = 8'h02;							//Dato
	#4 uartData = 8'h03;							//Dato
   #4 uartData = 8'h04;							//Dato
	#4 uartData = 8'h05;							//Dato
	#4 uartData = 8'h06;							//Dato
	#4 uartData = 8'h07;							//Dato
	#4 uartData = 8'h08;							//Dato*/
	#4 uartData = 8'hEF;
	
end

endmodule