module DataFeeder
#(
	parameter Word_Length = 8
)
(
	//Input ports
	input uartClk,
	input Clk_1kHz,
	input reset,
	input calculusReady,
	input push,
	input [Word_Length - 1 : 0] uartData,
	
	//Output ports
	output startCalculus,
	output retransmit,
	output pushFIFO1,
	output pushFIFO2,
	output pushFIFO3,
	output pushFIFO4,
	output pushFIFO5,
	output pushFIFO6,
	output pushFIFO7,
	output pushFIFO8,
	output pushVector,
	output [Word_Length - 1 : 0] dataFIFO1,
	output [Word_Length - 1 : 0] dataFIFO2,
	output [Word_Length - 1 : 0] dataFIFO3,
	output [Word_Length - 1 : 0] dataFIFO4,
	output [Word_Length - 1 : 0] dataFIFO5,
	output [Word_Length - 1 : 0] dataFIFO6,
	output [Word_Length - 1 : 0] dataFIFO7,
	output [Word_Length - 1 : 0] dataFIFO8,
	output [Word_Length - 1 : 0] dataVector,
	output [Word_Length - 1 : 0] matrixSize

);

bit generalFIFO_EmptyFlag;																//Bandera de FIFO que indica si esta vacia
bit syncReset_bit;																		//Reset sincrono para registros y contadores
bit individualFIFO_startFlag;															//Senial de control para comenzar la cuenta de counter individual
bit indivudualFIFO_countFlag;															//Bandera que indica si se llega a la cuenta de counter individual
bit generalFIFO_popSignal;																//Senial de control para hacer pop en general FIFO
bit oneShot_popSignal;																	//Senial para hacer pop en general FIFO con one shot
bit totaFIFO_countFlag;																	//Bandera que indica si ya llenamos las FIFOs de la matriz
bit pushIndividualFifo_bit;															//Senial de control para hacer push a las FIFOs individuales
bit enable_matrix_size_bit;															//Senial de control para habilitar el registro de tamanio de matriz
bit enableMatrixLoaded_bit;															//Senial de control para habilitar el registro de matrix loaded
bit enableVectorLoaded_bit;															//Senial de control para habilitar el registro de vector loaded
bit matrixLoaded_bit;																	//Bandera con la que sabemos si la matriz esta completamente cargada
bit vectorLoaded_bit;																	//Bandera con la que sabemos si el vector esta completamente cargados

logic [Word_Length - 5 : 0]demuxSelectorSignal;									//Selector para ambos demultiplexores

logic [Word_Length - 1 : 0] dataFromGeneralFIFO;								//Datos de salida de general FIFO
logic [Word_Length - 1 : 0] matrizSize;											//Tamanio de matriz

/****************************************************************/
FIFO
#(
	//Parameters
	//DATA_WIDTH: indicates the number of bits of the width
	.DATA_WIDTH(Word_Length), 
	//ADDR_WIDTH: indicates the number of bits of the address width
	.ADDR_WIDTH(Word_Length - 1)
	
)
GENERAL_FIFO
(
	//Input Ports
	.clk(uartClk),
	.reset(reset),
	.DataInput(uartData),
	.push(push),
	.pop(oneShot_popSignal),

	//Output Ports
	.DataOutput(dataFromGeneralFIFO),
	.full(),
	.empty(generalFIFO_EmptyFlag)
);

/****************************************************************/
OneShot
ONE_SHOT_POP_GENERALFIFO
(
	//Input Ports
	.clk(uartClk),
	.reset(reset),
	.Trigger(generalFIFO_popSignal & Clk_1kHz),
	
	//Output Ports
	.Pulse(oneShot_popSignal)
);

/****************************************************************/
DataFeederControl
#(
	.Word_Length(Word_Length)
)
DATAFEEDER_CONTROL_STATE_MACHINE
(
	//Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.individualFIFOCounterFlag(indivudualFIFO_countFlag),
	.totalFIFOsSizeFlag(totaFIFO_countFlag),
	.calculusReady(calculusReady),
	.matrixOrVector(matrixLoaded_bit),
	.isVectorLoaded(vectorLoaded_bit),
	.generalFIFO_emptyFlag(generalFIFO_EmptyFlag),
	.DataFromFIFO(dataFromGeneralFIFO),
	
	//Output Ports
	.matrixLoaded(enableMatrixLoaded_bit),
	.vectorLoaded(enableVectorLoaded_bit),
	.pushIndividualFifo(pushIndividualFifo_bit),
	.popGeneralFifo(generalFIFO_popSignal),
	.startindividualFIFOCounter(individualFIFO_startFlag),
	.startCalculus(startCalculus),
	.retransmit(retransmit),
	.syncReset(syncReset_bit),
	.enableMatrixSizeRegister(enable_matrix_size_bit),
	.demuxSelector(demuxSelectorSignal)

);

/****************************************************************/
CounterWithInput
#(
	// NBITS FOR COUNTER: Number of bits needed to represent the counter number.
	.NBITS_FOR_COUNTER(Word_Length)
)
INDIVIDUAL_FIFO_COUNTER
(
	// Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.syncReset(syncReset_bit),
	.enable(individualFIFO_startFlag),
	.COUNT_NUM(matrizSize),
	
	// Output Ports
	.flag(indivudualFIFO_countFlag)
);

/****************************************************************/
CounterWithInput
#(
	// NBITS FOR COUNTER: Number of bits needed to represent the counter number.
	.NBITS_FOR_COUNTER(Word_Length)
)
TOTAL_FIFO_COUNTER
(
	// Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.syncReset(syncReset_bit),
	.enable(indivudualFIFO_countFlag),
	.COUNT_NUM(matrizSize),
	
	// Output Ports
	.flag(totaFIFO_countFlag)
);

/****************************************************************/
Demux1to9
#(
	// Word_Length: Number of bits needed to represent the inputs and outputs.
	.Word_Length(Word_Length)
)
DEMUX_DATA_FIFOs
(
	// Input Ports
	.Data(dataFromGeneralFIFO),
	.Selector(demuxSelectorSignal),
	
	// Output Ports
	.Mux_Output_0(dataFIFO1),
	.Mux_Output_1(dataFIFO2),
	.Mux_Output_2(dataFIFO3),
	.Mux_Output_3(dataFIFO4),
	.Mux_Output_4(dataFIFO5),
	.Mux_Output_5(dataFIFO6),
	.Mux_Output_6(dataFIFO7),
	.Mux_Output_7(dataFIFO8),
	.Mux_Output_8(dataVector)
);

/****************************************************************/
Demux1to9
#(
	// Word_Length: Number of bits needed to represent the inputs and outputs.
	.Word_Length(1)
)
DEMUX_PUSH_FIFOs
(
	// Input Ports
	.Data(pushIndividualFifo_bit),
	.Selector(demuxSelectorSignal),
	
	// Output Ports
	.Mux_Output_0(pushFIFO1),
	.Mux_Output_1(pushFIFO2),
	.Mux_Output_2(pushFIFO3),
	.Mux_Output_3(pushFIFO4),
	.Mux_Output_4(pushFIFO5),
	.Mux_Output_5(pushFIFO6),
	.Mux_Output_6(pushFIFO7),
	.Mux_Output_7(pushFIFO8),
	.Mux_Output_8(pushVector)
);

/****************************************************************/
 Register_With_Clock_Enable
#(
	.WORD_LENGTH(Word_Length)
)
MATRIX_SIZE_REGISTER
(
	// Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.enable(enable_matrix_size_bit),
	.syncReset(syncReset_bit),
	.Data_Input(dataFromGeneralFIFO),

	// Output Ports
	.Data_Output(matrizSize)
);

/****************************************************************/
 Register_With_Clock_Enable
#(
	.WORD_LENGTH(Word_Length)
)
MATRIX_LOADED_REGISTER
(
	// Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.enable(enableMatrixLoaded_bit),
	.syncReset(syncReset_bit),
	.Data_Input(1),

	// Output Ports
	.Data_Output(matrixLoaded_bit)
);

/****************************************************************/
 Register_With_Clock_Enable
#(
	.WORD_LENGTH(Word_Length)
)
VECTOR_LOADED_REGISTER
(
	// Input Ports
	.clk(Clk_1kHz),
	.reset(reset),
	.enable(enableVectorLoaded_bit),
	.syncReset(syncReset_bit),
	.Data_Input(1),

	// Output Ports
	.Data_Output(vectorLoaded_bit)
);

assign matrixSize = matrizSize;
/*assign dataFIFO1 = dataFromGeneralFIFO;
assign dataFIFO2 = dataFromGeneralFIFO;
assign dataFIFO3 = dataFromGeneralFIFO;
assign dataFIFO4 = dataFromGeneralFIFO;
assign dataFIFO5 = dataFromGeneralFIFO;
assign dataFIFO6 = dataFromGeneralFIFO;
assign dataFIFO7 = dataFromGeneralFIFO;
assign dataFIFO8 = dataFromGeneralFIFO;
assign dataVector = dataFromGeneralFIFO;
*/

endmodule 