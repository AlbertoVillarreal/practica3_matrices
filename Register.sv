module Register
#(
	// WORD_LENGTH: Number of bits needed to represent the inputs and outputs.
	parameter WORD_LENGTH = 8
)
(
	// Input Ports
	input clk,
	input reset,
	input syncReset,
	input [WORD_LENGTH-1:0] data,
	input enable, 
	
	// Output Ports
	output logic [WORD_LENGTH-1:0] result
);

/*	Sequential. It makes two's complement if it is needed and also shift right or left the data */
always_ff@(posedge clk, negedge reset) 
begin

	if(reset == 1'b0) 
		result <= 0;
		
	else if(syncReset == 1'b1)
		result <= 0;
	
	else
		begin
			if(enable == 1'b1)	
				result <= data;
						
			else
				result <= result;
		end
end

endmodule 