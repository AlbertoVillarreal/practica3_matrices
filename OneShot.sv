module OneShot
(
	// Input Ports
	input clk,
	input reset,
	input Trigger,
	
	// Output Ports
	output Pulse
);

wire First_FlipFlop_output;
wire Second_FilpFlop_output;

FlipFlop_D
First_FlipFlop
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.data(Trigger),
	
	// Output Ports
	.q(First_FlipFlop_output)
);

FlipFlop_D
Second_FlipFlop
(
	// Input Ports
	.clk(clk),
	.reset(reset),
	.data(First_FlipFlop_output),
	
	// Output Ports
	.q(Second_FilpFlop_output)
);

assign Pulse = (~Second_FilpFlop_output) & First_FlipFlop_output;

endmodule
